<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tu".
 *
 * @property int $id
 * @property string $title
 *
 * @property FhpLim[] $fhpLims
 * @property Pack[] $packs
 * @property PackFalse[] $packFalses
 */
class Tu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 160],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Тех. условия',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFhpLims()
    {
        return $this->hasMany(FhpLim::className(), ['id_tu' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacks()
    {
        return $this->hasMany(Pack::className(), ['id_tu' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackFalses()
    {
        return $this->hasMany(PackFalse::className(), ['id_tu' => 'id']);
    }




    protected function getFHP(array $data){

        return $result = FhpLim::findOne([
            'id_marka' => $data['marka'],
            'id_tu' => $data['tu']
        ]);

    }

    /**
     * Проверка поля на валидность, если валидное то true
     * @param array $data
     * @return boolean
     */
    public function validFHPUdel(array $data)
    {
        $fhp = $this->getFHP($data);

        if ($data['ar'] != '') {
            if (($data['ar'] >= $fhp['udel_min'])){
                $data['ar'] = 1;
            }else{
                $data['ar'] = 0;
            }
        }else{
            $data['ar'] = 2;
        }

        if ($data['az'] != '') {
            if (($data['az'] >= $fhp['udel_min'])){
                $data['az'] = 1;
            }else{
                $data['az'] = 0;
            }
        }else{
            $data['az'] = 2;
        }

        return json_encode(array($data));
    }


    /**
     * Проверка поля на валидность, если валидное то true
     * @param array $data
     * @return boolean
     */
    public function validFHP(array $data)
    {
        $fhp = $this->getFHP($data);

        switch ($data['field']) {

            case 'diam':
                if (($data['value'] >= $fhp['diam_min']) and ($data['value'] <= $fhp['diam_max'])){return true;}
                break;

            case 'massdol':
                if (($data['value'] >= $fhp['massdol_min'])){return true;}
                break;

            case 'nasyp':
                if (($data['value'] >= $fhp['nasyp_min']) and ($data['value'] <= $fhp['nasyp_max'])){return true;}
                break;

            case 'massdolprokal':
                if (($data['value'] <= $fhp['massdolprokal_max'])){return true;}
                break;

            case 'mech':
                if (($data['value'] >= $fhp['mech_min'])){return true;}
                break;

            case 'istir':
                if (($data['value'] <= $fhp['istir_max'])){return true;}
                break;

            case 'comob':
                if (($data['value'] >= $fhp['comob_min'])){return true;}
                break;

            default:
                break;
        }

        return false;

    }

}
