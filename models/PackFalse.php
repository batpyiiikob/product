<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pack_false".
 *
 * @property int $id
 * @property string $date_create
 * @property string $date_modify
 * @property string $number
 * @property int $id_tu
 * @property int $id_marka
 * @property int $id_status
 * @property int $id_sklad
 * @property int $id_smena
 * @property double $fhp_diam
 * @property double $fhp_massdol
 * @property double $fhp_nasyp
 * @property double $fhp_massdolprokal
 * @property double $fhp_udel_azot
 * @property double $fhp_udel_argon
 * @property double $fhp_mech
 * @property double $fhp_istir
 * @property double $fhp_comob
 * @property string $comment
 *
 * @property Marka $marka
 * @property Sklad $sklad
 * @property Smena $smena
 * @property Status $status
 * @property Tu $tu
 */
class PackFalse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pack_false';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_create', 'date_modify'], 'safe'],
            [['number'], 'required'],
            [['id_tu', 'id_smena', 'id_marka', 'id_status', 'id_sklad'], 'integer'],
            [['fhp_diam', 'fhp_massdol', 'fhp_nasyp', 'fhp_massdolprokal', 'fhp_udel_azot', 'fhp_udel_argon', 'fhp_mech', 'fhp_istir', 'fhp_comob'], 'number'],
            [['comment'], 'string'],
            [['number'], 'string', 'max' => 255],
            [['id_marka'], 'exist', 'skipOnError' => true, 'targetClass' => Marka::className(), 'targetAttribute' => ['id_marka' => 'id']],
            [['id_sklad'], 'exist', 'skipOnError' => true, 'targetClass' => Sklad::className(), 'targetAttribute' => ['id_sklad' => 'id']],
            [['id_status'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['id_status' => 'id']],
            [['id_smena'], 'exist', 'skipOnError' => true, 'targetClass' => Smena::className(), 'targetAttribute' => ['id_smena' => 'id']],
            [['id_tu'], 'exist', 'skipOnError' => true, 'targetClass' => Tu::className(), 'targetAttribute' => ['id_tu' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_create' => 'Дата и время регистрации',
            'date_modify' => 'Дата и время изменения',
            'number' => 'Номер мешка',
            'id_tu' => 'ТУ',
            'id_smena' => 'Смена',
            'id_marka' => 'Марка продукта',
            'id_status' => 'Статус',
            'id_sklad' => 'Склад',
            'fhp_diam' => 'Диаметр гранул, мм',
            'fhp_massdol' => 'Массовая доля гранул заданного диаметра, %',
            'fhp_nasyp' => 'Насыпная плотность, г/см3',
            'fhp_massdolprokal' => 'Массовая доля потерь при прокалывании, %',
            'fhp_udel_azot'   => 'Удельная поверхность по АЗОТУ, м2/г',
            'fhp_udel_argon'  => 'Удельная поверхность по АРГОНУ, м2/г',
            'fhp_mech' => 'Механическая прочность при раздавливании',
            'fhp_istir' => 'Истираемость поверхности слоя',
            'fhp_comob' => 'Общий объём пор, см3/г',
            'comment' => 'Примечание',
            //'user' => 'Пользователь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarka()
    {
        return $this->hasOne(Marka::className(), ['id' => 'id_marka']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSklad()
    {
        return $this->hasOne(Sklad::className(), ['id' => 'id_sklad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmena()
    {
        return $this->hasOne(Smena::className(), ['id' => 'id_smena']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTu()
    {
        return $this->hasOne(Tu::className(), ['id' => 'id_tu']);
    }
}
