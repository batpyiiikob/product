<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "part".
 *
 * @property int $id
 * @property string $date_create
 * @property string $number
 * @property int $id_marka
 * @property int $id_sklad
 * @property int $id_partner
 * @property int $id_tu
 * @property string $date_out
 * @property int $id_status
 * @property string $comment
 *
 * @property Marka $marka
 * @property Partner $partner
 * @property Sklad $sklad
 * @property Status $status
 * @property PartPack[] $partPacks
 */
class Part extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'part';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_create', 'date_out'], 'safe'],
            [['number','id_marka', 'id_tu'], 'required'],
            [['id_marka', 'id_sklad', 'id_partner', 'id_tu', 'id_status'], 'integer'],
            //[['id_sklad', 'id_partner', 'id_status'], 'filter', 'filter' => 'intval'], // для getDirtyAttributes
            [['id_sklad', 'id_status'], 'filter', 'filter' => 'intval'], // для getDirtyAttributes
            [['number', 'comment'], 'string', 'max' => 255],
            [['number'], 'unique'],
            [['id_marka'], 'exist', 'skipOnError' => true, 'targetClass' => Marka::className(), 'targetAttribute' => ['id_marka' => 'id']],
            [['id_partner'], 'exist', 'skipOnError' => true, 'targetClass' => Partner::className(), 'targetAttribute' => ['id_partner' => 'id']],
            [['id_sklad'], 'exist', 'skipOnError' => true, 'targetClass' => Sklad::className(), 'targetAttribute' => ['id_sklad' => 'id']],
            [['id_status'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['id_status' => 'id']],
            [['id_tu'], 'exist', 'skipOnError' => true, 'targetClass' => Tu::className(), 'targetAttribute' => ['id_tu' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_create' => 'Дата и время регистрации',
            'number' => 'Номер партии',
            'id_marka' => 'Марка продукции',
            'id_sklad' => 'Склад',
            'id_partner' => 'Партнёр',
            'date_out' => 'Дата отгрузки',
            'id_status' => 'Статус',
            'id_tu' => 'ТУ',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarka()
    {
        return $this->hasOne(Marka::className(), ['id' => 'id_marka']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partner::className(), ['id' => 'id_partner']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSklad()
    {
        return $this->hasOne(Sklad::className(), ['id' => 'id_sklad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTu()
    {
        return $this->hasOne(Tu::className(), ['id' => 'id_tu']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(StatusPart::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartPacks()
    {
        return $this->hasMany(PartPack::className(), ['id_part' => 'id']);
    }




    private function getPacksArrayByPart($id){

        // По id партии ищем все мешки в пакетах
        $id_packs_arr = Packinpart::find()
            ->select('id_pack')
            ->where(['id_part' => $id])
            ->asArray()
            ->all();

        // выбор всех id мешков которые в пакете в числовой массив id - $id_packs
        $id_packs = [];
        foreach ($id_packs_arr as $value){
            $id_packs[] = (int)$value['id_pack'];
        }

        return $id_packs;
    }




    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            // Получить id пакета
            // Получить массив id мешков в пакете
            // Получить доступ к мешкам
            // Массив мешков передать в updateAll для изменения склада

            $newSklad =  $this->getDirtyAttributes(['id_sklad']);

            if ( !empty($newSklad) ){ // id_sklad или 0, или пусто, или вообще не определена (НЕ)';
            // было изменение класса - изменить склад всех мешков
                $id_packs = $this->getPacksArrayByPart($this->id);
                Pack::updateAll(['id_sklad'=>$newSklad['id_sklad']], ['id'=>$id_packs]);
            }

            $newStatus =  $this->getDirtyAttributes(['id_status']);

            if ( !empty($newStatus) && ($newStatus['id_status'] == 5) ){ // id_status или 0, или пусто, или вообще не определена (НЕ)';
            // было изменение статуса реализовано - изменить статус всех мешков и удалить их со складов
                $id_packs = $this->getPacksArrayByPart($this->id);
                Pack::updateAll(['id_status'=> 7, 'id_sklad'=> ''], ['id'=>$id_packs]);
                $this->id_sklad = '';
            }

            return true;
        } else {
            return false;
        }

    }



    /**
     * @return $number
     */
    static function getNumber($id)
    {
        $liters = [
            '18' => '18',
            '19' => 'A',
            '20' => 'B',
            '21' => 'C',
            '22' => 'E',
            '23' => 'F',
            '24' => 'G',
            '25' => 'H',
            '26' => 'K',
            '27' => 'M',
            '28' => 'N',
            '29' => 'P',
            '30' => 'R',
            '31' => 'S',
            '32' => 'T',
            '33' => 'U',
            '34' => 'V',
            '35' => 'W',
            '36' => 'X',
            '37' => 'Y',
            '38' => 'Z'
        ];

        // получаем текущий год
        $year = substr((new \DateTime('now', new \DateTimeZone('Europe/Moscow')))->format('Y'), 2, 4);

        // получаем литеру соответствующую году
        if (array_key_exists($year, $liters)) {
            $liter = $liters[$year];
        } else {
            $liter = 'AA';
        }

        $number = Part::find()
            ->select(['number'])
            ->where(['id_marka' => $id])
            ->andWhere(['like', 'number', $liter])
            ->orderBy('number')
            ->asArray()
            ->all();


        $marka = Marka::findOne($id)->title;

        if ($number == null) {
            $number = 1;
        } else {
            $number = end($number)['number'];
            $number = substr($number, 0, 4) + 1;
        }

        if ($number != 1) {
            switch (strlen($number)) {
                case 1:
                    $number = '000'.$number;
                    break;
                case 2:
                    $number = '00'.$number;
                    break;
                case 3:
                    $number = '0'.$number;
                    break;
            }
            $number .= '/' . $liter. '/'.$marka;
        }else{
            $number = '0001/' . $liter. '/'.$marka;
        }

        return $number;
    }




}
