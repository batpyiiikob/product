<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marka".
 *
 * @property int $id
 * @property string $title
 *
 * @property FhpLim[] $fhpLims
 * @property Pack[] $packs
 * @property PackFalse[] $packFalses
 * @property Part[] $parts
 */
class Marka extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marka';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 160],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Марка продукта',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFhpLims()
    {
        return $this->hasMany(FhpLim::className(), ['id_marka' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacks()
    {
        return $this->hasMany(Pack::className(), ['id_marka' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackFalses()
    {
        return $this->hasMany(PackFalse::className(), ['id_marka' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParts()
    {
        return $this->hasMany(Part::className(), ['id_marka' => 'id']);
    }


  }
