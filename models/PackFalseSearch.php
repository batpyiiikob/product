<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PackFalse;

/**
 * PackSearch represents the model behind the search form of `app\models\Pack`.
 */
class PackFalseSearch extends PackFalse
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_tu', 'id_marka', 'id_status', 'id_sklad', 'id_smena'], 'integer'],
            [['date_create', 'date_modify', 'number', 'comment'], 'safe'],
            [['fhp_diam', 'fhp_massdol', 'fhp_nasyp', 'fhp_massdolprokal', 'fhp_udel_azot', 'fhp_udel_argon', 'fhp_mech', 'fhp_istir', 'fhp_comob'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PackFalse::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_create' => $this->date_create,
            'date_modify' => $this->date_modify,
            'id_tu' => $this->id_tu,
            'id_marka' => $this->id_marka,
            'id_status' => $this->id_status,
            'id_sklad' => $this->id_sklad,
            'id_smena' => $this->id_smena,
            'fhp_diam' => $this->fhp_diam,
            'fhp_massdol' => $this->fhp_massdol,
            'fhp_nasyp' => $this->fhp_nasyp,
            'fhp_massdolprokal' => $this->fhp_massdolprokal,
            'fhp_udel_azot' => $this->fhp_udel_azot,
            'fhp_udel_argon' => $this->fhp_udel_argon,
            'fhp_mech' => $this->fhp_mech,
            'fhp_istir' => $this->fhp_istir,
            'fhp_comob' => $this->fhp_comob,
        ]);

        $query->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
