<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pack".
 *
 * @property int $id
 * @property string $date_create
 * @property string $date_modify
 * @property string $number
 * @property int $id_tu
 * @property int $id_marka
 * @property int $id_status
 * @property int $id_sklad
 * @property int $id_smena
 * @property double $fhp_diam
 * @property double $fhp_massdol
 * @property double $fhp_nasyp
 * @property double $fhp_massdolprokal
 * @property double $fhp_udel_azot
 * @property double $fhp_udel_argon
 * @property double $fhp_mech
 * @property double $fhp_istir
 * @property double $fhp_comob
 * @property string $comment
 *
 * @property Marka $marka
 * @property Part $part
 * @property Sklad $sklad
 * @property Status $status
 * @property Smena $smena
 * @property Tu $tu
 * @property PartPack[] $partPacks
 */
class Pack extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pack';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_create', 'date_modify'], 'safe'],
            [['id_tu', 'id_smena', 'id_marka', 'id_status', 'id_sklad'], 'integer'],
            [['fhp_diam', 'fhp_massdol', 'fhp_nasyp', 'fhp_massdolprokal', 'fhp_udel_azot', 'fhp_udel_argon', 'fhp_mech', 'fhp_istir', 'fhp_comob'], 'number'],
            [['comment'], 'string'],
            [['number'], 'required'],
            [['number'], 'string', 'max' => 24],
            [['number'], 'unique'],
            [['id_marka'], 'exist', 'skipOnError' => true, 'targetClass' => Marka::className(), 'targetAttribute' => ['id_marka' => 'id']],
            [['id_marka'], 'required'],
            [['id_tu'], 'required'],
            [['id_sklad'], 'exist', 'skipOnError' => true, 'targetClass' => Sklad::className(), 'targetAttribute' => ['id_sklad' => 'id']],
            [['id_status'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['id_status' => 'id']],
            [['id_smena'], 'exist', 'skipOnError' => true, 'targetClass' => Smena::className(), 'targetAttribute' => ['id_smena' => 'id']],
            [['id_tu'], 'exist', 'skipOnError' => true, 'targetClass' => Tu::className(), 'targetAttribute' => ['id_tu' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_create' => 'Дата и время регистрации',
            'date_modify' => 'Дата и время изменения',
            'number' => 'Номер мешка',
            'id_tu' => 'ТУ',
            'id_smena' => 'Смена',
            'id_marka' => 'Марка продукта',
            'id_status' => 'Статус',
            'id_sklad' => 'Склад',
            'fhp_diam' => 'Диаметр гранул, мм',
            'fhp_massdol' => 'Массовая доля гранул заданного диаметра, %',
            'fhp_nasyp' => 'Насыпная плотность, г/см3',
            'fhp_massdolprokal' => 'Массовая доля потерь при прокалывании, %',
            'fhp_udel_azot'   => 'Удельная поверхность по АЗОТУ, м2/г',
            'fhp_udel_argon'  => 'Удельная поверхность по АРГОНУ, м2/г',
            'fhp_mech' => 'Механическая прочность при раздавливании',
            'fhp_istir' => 'Истираемость поверхности слоя',
            'fhp_comob' => 'Общий объём пор, см3/г',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarka()
    {
        return $this->hasOne(Marka::className(), ['id' => 'id_marka']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSklad()
    {
        return $this->hasOne(Sklad::className(), ['id' => 'id_sklad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'id_status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmena()
    {
        return $this->hasOne(Smena::className(), ['id' => 'id_smena']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTu()
    {
        return $this->hasOne(Tu::className(), ['id' => 'id_tu']);
    }



    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $packfalse = new PackFalse();

            $packfalse->date_create = $this->date_create;
            $packfalse->date_modify = $this->date_modify;
            $packfalse->number = $this->number;
            $packfalse->id_smena = $this->id_smena;
            $packfalse->id_tu = $this->id_tu;
            $packfalse->id_marka = $this->id_marka;
            $packfalse->id_status = $this->id_status;
            $packfalse->id_sklad = $this->id_sklad;
            $packfalse->fhp_diam = $this->fhp_diam;
            $packfalse->fhp_massdol = $this->fhp_massdol;
            $packfalse->fhp_nasyp = $this->fhp_nasyp;
            $packfalse->fhp_udel_azot = $this->fhp_udel_azot;
            $packfalse->fhp_udel_argon = $this->fhp_udel_argon;
            $packfalse->fhp_massdolprokal = $this->fhp_massdolprokal;
            $packfalse->fhp_mech = $this->fhp_mech;
            $packfalse->fhp_istir = $this->fhp_istir;
            $packfalse->fhp_comob = $this->fhp_comob;
            $packfalse->comment = $this->comment;
            $packfalse->save();
            //Yii::$app->session->setFlash('success', 'Логирование успешно завершено!');
            return true;

        } else {

            return false;

        }

    }

    /**
     *
     * @param array $model
     *
     * @return $fhpArray
     */
    static function fhpLabelsErrors($model)
    {
        $mess = Yii::$app->params['paramTuFalse'];
        $messUdel = Yii::$app->params['paramTuFalseUdel'];

        $fhpArray = [
            'diam'=>$mess,
            'massdol'=>$mess,
            'nasyp'=>$mess,
            'massdolprokal'=>$mess,
            'udel_azot'=>$messUdel,
            'udel_argon'=>$messUdel,
            'mech'=>$mess,
            'istir'=>$mess,
            'comob'=>$mess,
        ];

        $fhplim = FhpLim::findOne([
            'id_marka' => $model->marka->id,
            'id_tu' => $model->tu->id,
        ]);

        if (($model->fhp_diam >= $fhplim['diam_min']) and ($model->fhp_diam <= $fhplim['diam_max'])) {
            $fhpArray['diam'] = '';
        }

        if (($model->fhp_massdol >= $fhplim['massdol_min'])) {
            $fhpArray['massdol'] = '';
        }

        if (($model->fhp_nasyp >= $fhplim['nasyp_min']) and ($model->fhp_nasyp <= $fhplim['nasyp_max'])) {
            $fhpArray['nasyp'] = '';
        }

        if (($model->fhp_massdolprokal <= $fhplim['massdolprokal_max'])) {
            $fhpArray['massdolprokal'] = '';
        }



        /*
         *
         * 3 состояния поля аргона, в низ 3 состояния поля азота
         *
         */

        $fieldArgon = $model->fhp_udel_argon;
        switch ($fieldArgon){
            case '':

                if (($model->fhp_udel_azot >= $fhplim['udel_min'])) {
                    $fhpArray['udel_azot'] = ''; //true
                }else{
                    $fhpArray['udel_azot'] = $mess; //false
                }
                $fhpArray['udel_argon'] = '';
                break;

            case ($fieldArgon >= $fhplim['udel_min']):  //true

                if (($model->fhp_udel_azot >= $fhplim['udel_min'])) {
                    $fhpArray['udel_azot'] = ''; //true
                }else{
                    $fhpArray['udel_azot'] = $messUdel; //false
                }
                $fhpArray['udel_argon'] = '';
                break;

            case ($fieldArgon < $fhplim['udel_min']):  //false

                if (($model->fhp_udel_azot >= $fhplim['udel_min'])) {
                    $fhpArray['udel_azot'] = ''; //true
                }else{
                    $fhpArray['udel_azot'] = $messUdel; //false
                }
                $fhpArray['udel_argon'] = $mess;
                break;
        }


        if (($model->fhp_udel_azot >= $fhplim['udel_min'])) {
            $fhpArray['udel_azot'] = ''; //true
        }

        if (($model->fhp_udel_argon >= $fhplim['udel_min'])) {
            $fhpArray['udel_argon'] = ''; //true
        }

        if (($model->fhp_mech >= $fhplim['mech_min'])) {
            $fhpArray['mech'] = '';
        }

        if (($model->fhp_istir <= $fhplim['istir_max'])) {
            $fhpArray['istir'] = '';
        }

        if (($model->fhp_comob >= $fhplim['comob_min'])) {
            $fhpArray['comob'] = '';
        }

        return $fhpArray;
    }




    /**
     *
     * @param integer $id - номер марки 1 - 13
     *
     * @return string $number
     */
    static function getNumber($id)
    {
        $liters = [
            '18' => '18',
            '19' => 'A',
            '20' => 'B',
            '21' => 'C',
            '22' => 'E',
            '23' => 'F',
            '24' => 'G',
            '25' => 'H',
            '26' => 'K',
            '27' => 'M',
            '28' => 'N',
            '29' => 'P',
            '30' => 'R',
            '31' => 'S',
            '32' => 'T',
            '33' => 'U',
            '34' => 'V',
            '35' => 'W',
            '36' => 'X',
            '37' => 'Y',
            '38' => 'Z'
        ];

        // получаем текущий год
        $year = substr((new \DateTime('now', new \DateTimeZone('Europe/Moscow')))->format('Y'), 2, 4);

        // получаем литеру соответствующую году
        if (array_key_exists($year, $liters)) {
            $liter = $liters[$year];
        } else {
            $liter = 'AA';
        }

        $number = Pack::find()
            ->select(['number'])
            ->where(['id_marka' => $id])
            ->andWhere(['like', 'number', $liter])
            ->orderBy('number')
            ->asArray()
            ->all();


        $marka = Marka::findOne($id)->title;

        if ($number == null) {
            $number = 1;
        } else {
            $number = end($number)['number'];
            $number = substr($number, 0, 4) + 1;
        }

        if ($number != 1) {
            switch (strlen($number)) {
                case 1:
                    $number = '000'.$number;
                    break;
                case 2:
                    $number = '00'.$number;
                    break;
                case 3:
                    $number = '0'.$number;
                    break;
            }
            $number .= '/' . $liter. '/'.$marka;
        }else{
            $number = '0001/' . $liter. '/'.$marka;
        }

        return $number;
    }


    static function changeSklad($array){
        // если не соответстует ТУ и выбран склад Цеха, то сменить статус на переработку
        if (($array['id_status'] == 3) and ($array['value'] == 1)){
            return $status = 4;
        }else{
            return $array['id_status'];
        }

    }





}
