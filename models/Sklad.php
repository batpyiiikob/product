<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sklad".
 *
 * @property int $id
 * @property string $title
 *
 * @property Pack[] $packs
 * @property PackFalse[] $packFalses
 * @property Part[] $parts
 */
class Sklad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sklad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 160],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование склада',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacks()
    {
        return $this->hasMany(Pack::className(), ['id_sklad' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackFalses()
    {
        return $this->hasMany(PackFalse::className(), ['id_sklad' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParts()
    {
        return $this->hasMany(Part::className(), ['id_sklad' => 'id']);
    }
}
