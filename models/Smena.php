<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "smena".
 *
 * @property int $id
 * @property string $title
 *
 * @property Pack[] $packs
 * @property PackFalse[] $packFalses
 */
class Smena extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'smena';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 160],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование смены',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPacks()
    {
        return $this->hasMany(Pack::className(), ['id_smena' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackFalses()
    {
        return $this->hasMany(PackFalse::className(), ['id_smena' => 'id']);
    }
}
