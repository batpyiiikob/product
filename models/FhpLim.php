<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fhp_lim".
 *
 * @property int $id
 * @property int $id_marka
 * @property int $id_tu
 * @property double $diam_min
 * @property double $diam_max
 * @property double $massdol_min
 * @property double $nasyp_min
 * @property double $nasyp_max
 * @property double $massdolprokal_max
 * @property double $udel_min
 * @property double $mech_min
 * @property double $istir_max
 * @property double $comob_min
 * @property string $comment
 *
 * @property Marka $marka
 * @property Tu $tu
 */
class FhpLim extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fhp_lim';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_marka', 'id_tu'], 'integer'],
            [['diam_min', 'diam_max', 'massdol_min', 'nasyp_min', 'nasyp_max', 'massdolprokal_max', 'udel_min', 'mech_min', 'istir_max', 'comob_min'], 'number'],
            [['comment'], 'string', 'max' => 255],
            [['id_marka'], 'exist', 'skipOnError' => true, 'targetClass' => Marka::className(), 'targetAttribute' => ['id_marka' => 'id']],
            [['id_tu'], 'exist', 'skipOnError' => true, 'targetClass' => Tu::className(), 'targetAttribute' => ['id_tu' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_marka' => 'Марка',
            'id_tu' => 'ТУ',
            'diam_min' => 'Диаметр гранул, мм - Min',
            'diam_max' => 'Диаметр гранул, мм - Max',
            'massdol_min' => 'Массовая доля гранул заданного диаметра, мм - Min',
            'nasyp_min' => 'Насыпная плотность, г/см3 - Min',
            'nasyp_max' => 'Насыпная плотность, г/см3 - Max',
            'massdolprokal_max' => 'Массовая доля потерь при прокалывании, % - Max',
            'udel_min' => 'Удельная поверхность, м2/г - Min',
            'mech_min' => 'Механическая прочность при раздавливании, МПа - Min',
            'istir_max' => 'Истираемость поверхности слоя, % - Max',
            'comob_min' => 'Общий объём пор, см3/г - Min',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarka()
    {
        return $this->hasOne(Marka::className(), ['id' => 'id_marka']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTu()
    {
        return $this->hasOne(Tu::className(), ['id' => 'id_tu']);
    }
}
