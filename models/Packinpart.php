<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "packinpart".
 *
 * @property int $id
 * @property int $id_part
 * @property int $id_pack
 *
 *
 * @property Pack $pack
 * @property Part $part
 * @property NumberPart $numberPart
 * @property NumberPack $numberPack
 * @property DataCreatedPack $dataCreatedPack
 *
 */
class Packinpart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'packinpart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_part', 'id_pack'], 'required'],
            [['id_part', 'id_pack'], 'integer'],
            [['id_pack'], 'exist', 'skipOnError' => true, 'targetClass' => Pack::className(), 'targetAttribute' => ['id_pack' => 'id']],
            [['id_part'], 'exist', 'skipOnError' => true, 'targetClass' => Part::className(), 'targetAttribute' => ['id_part' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_part' => 'ID Партии',
            'id_pack' => 'ID Мешка',
            'dataCreatedPack' => 'Дата производства продукта',
            'numberPack' => 'Номер мешка',
            'numberPart' => 'Номер партии'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPack()
    {
        return $this->hasOne(Pack::className(), ['id' => 'id_pack']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPart()
    {
        return $this->hasOne(Part::className(), ['id' => 'id_part']);
    }

    /**
     * @return string
     */
    public function getNumberPart()
    {

        return Part::findOne($this->id_part)->number;

    }


    /**
     * @return string
     */
    public function getNumberPack()
    {

        return Pack::findOne($this->id_pack)->number;

    }

    /**
     * @return string
     */
    public function getDataCreatedPack()
    {

        return Pack::findOne($this->id_pack)->date_create;

    }
}
