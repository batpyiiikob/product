<?php

namespace app\controllers;

use Yii;
use app\models\Part;
use app\models\Pack;
use app\models\Packinpart;
use app\models\PartSearch;
use app\models\PackSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
/**
 * PartController implements the CRUD actions for Part model.
 */
class PartController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
          return [
              'access' => [
                  'class' => AccessControl::className(),
                  'only' => ['logout', 'index'],
                  'rules' => [
                      [
                          'actions' => ['logout', 'index'],
                          'allow' => true,
                          'roles' => ['@'], //аутентифицированным пользователям доступ к действию logout разрешить
                      ],
                      [
                          'actions' => ['index'],
                          'allow' => false,
                          'roles' => ['?'],
                      ],
                  ],
              ],
              'verbs' => [
                  'class' => VerbFilter::className(),
                  'actions' => [
                      'delete' => ['POST'],
                  ],
              ],
          ];
      }



    /**
     * Lists all Part models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PartSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider -> sort -> defaultOrder = [
            'date_create' => SORT_DESC
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Part model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        $dataProvider = new ActiveDataProvider([
            'query' => Packinpart::find()->where(['id_part' => $id]),
            'pagination' => false
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * Creates a new Part model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Part();

        $dataProvider = new ActiveDataProvider([
            'query' => Pack::find(),
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing Part model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Part model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Part model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Part the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Part::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



    public function actionNumPart()
    {
        //if(Yii::$app->request->isAjax)
        $id = (int)Yii::$app->request->post('id');
        $result = Part::getNumber($id);

        return $result;
    }





//Обработки склад____________________________________________________________________________________________________
    public function actionKlad()
    {
        $searchModel = new PartSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider -> sort -> defaultOrder = [
            'date_create' => SORT_DESC
        ];

        return $this->render('klad/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionKladUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['klad']);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Packinpart::find()->where(['id_part' => $id]),
            'pagination' => false
        ]);

        return $this->render('klad/update', [
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }








//Обработки лаборатория____________________________________________________________________________________________________
    public function actionLab()
    {
        $searchModel = new PartSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider -> sort -> defaultOrder = [
            'date_create' => SORT_DESC
        ];

        return $this->render('lab/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


   public function actionLabCreate()
    {
        $model = new Part();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['lab']);
        }

        return $this->render('lab/create', [
            'model' => $model,
        ]);
    }

    public function actionLabUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['lab']);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Packinpart::find()->where(['id_part' => $id]),
            'pagination' => false
        ]);

        return $this->render('lab/update', [
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    public function actionLabView($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Packinpart::find()->where(['id_part' => $id]),
            'pagination' => false
        ]);

        return $this->render('lab/view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
        ]);
    }

}
