<?php

namespace app\controllers;

use app\models\PackSearch;
use app\models\Pack;
use app\models\PackFalse;
use app\models\Part;
use Yii;
use app\models\Packinpart;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PackinpartController implements the CRUD actions for Packinpart model.
 */
class PackinpartController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Packinpart models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Packinpart::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Packinpart model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Packinpart model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Packinpart();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Packinpart model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Packinpart model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $pack = Pack::findOne(Packinpart::findOne($id)->id_pack);

        $packFalse = (PackFalse::find()
            ->where(['number' => $pack->number])
            ->orderBy(['id'=>SORT_DESC])
            ->one());
        $pack->id_status = $packFalse->id_status;
        $pack->update();

        $this->findModel($id)->delete();

        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['add']);
        }
    }


    /**
     * Adding an existing Part packs.
     * id part
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAdd($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Packinpart::find()->where(['id_part' => $id]),
            'pagination' => false
        ]);

        $searchPackModel = new PackSearch();

        $dataProviderPacks = $searchPackModel->search(Yii::$app->request->queryParams);
        $dataProviderPacks->query
            ->andWhere(['id_status' => [2,3]])
            ->andWhere(['id_sklad' => [2,3]])
            ->andWhere(['id_tu' => Part::findOne($id)->id_tu])
            ->andWhere(['id_marka' => Part::findOne($id)->id_marka]);

        return $this->render('add', [
            'partNumber' => Part::findOne($id)->number, // в actionChange()
            'partID' => Part::findOne($id)->id, // в actionChange()
            'dataProvider' => $dataProvider,
            'searchPackModel' => $searchPackModel,
            'dataProviderPacks' => $dataProviderPacks,
        ]);

    }


    public function actionChange()
    {

        if (\Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post('keys');
            $id_part = Yii::$app->request->post('id');

            Pack::updateAll(['id_status'=>6], ['id'=>$data]);

            foreach ($data as $value){
                $packinpart[] = [ $id_part, $value ];
            }

            Yii::$app->db->createCommand()
                ->batchInsert(Packinpart::tableName(), ['id_part', 'id_pack'], $packinpart)
                ->execute();

            return false;

        }

    }




    /**
     * Finds the Packinpart model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Packinpart the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Packinpart::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
