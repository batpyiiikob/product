<?php

namespace app\controllers;

use yii\data\ActiveDataProvider;
use app\models\PackFalse;
use Yii;
use app\models\Pack;
use app\models\PackSearch;
use app\models\PackFalseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PackController implements the CRUD actions for Pack model.
 */
class PackController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'], //аутентифицированным пользователям доступ к действию logout разрешить
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pack models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider -> sort -> defaultOrder = [
            'date_create' => SORT_DESC
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pack model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pack model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pack();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Pack model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Pack model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pack model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pack the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pack::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }



    public function actionNumPack()
    {
        if(Yii::$app->request->isAjax)
        {
            $id = (int)Yii::$app->request->post('id');
            $result = Pack::getNumber($id);

            return $result;
        }
        return 0;
    }


//Обработки сменщики____________________________________________________________________________________________________
    public function actionSmena()
    {
        $searchModel = new PackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider -> sort -> defaultOrder = [
            'date_create' => SORT_DESC
        ];

        return $this->render('smena/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSmenaCreate()
    {
        $model = new Pack();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['smena']);
        }

        return $this->render('smena/create', [
            'model' => $model,
        ]);
    }

    public function actionSmenaUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['smena']);
        }

        return $this->render('smena/update', [
            'model' => $model,
        ]);
    }

    public function actionSmenaView($id)
    {
        return $this->render('smena/view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionSmenaDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['smena']);
    }







//Обработки кладовщики____________________________________________________________________________________________________
    public function actionKlad()
    {
        $searchModel = new PackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider -> sort -> defaultOrder = [
            'date_create' => SORT_DESC
        ];

        return $this->render('klad/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionKladUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['klad']);
        }

        return $this->render('klad/update', [
            'model' => $model,
        ]);
    }

    public function actionKladView($id)
    {
        return $this->render('klad/view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionKladDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['klad']);
    }


    public function actionChangeSklad()
    {
        if(Yii::$app->request->isAjax) {
            $result = Yii::$app->request->post();
            return Pack::changeSklad($result);
        }
        else{
            return 0;
        }
    }





//Обработки лаборатория____________________________________________________________________________________________________
    public function actionLab()
    {
        $searchModel = new PackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider -> sort -> defaultOrder = [
            'date_create' => SORT_DESC
        ];

        return $this->render('lab/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionLabUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['lab']);
        }

        return $this->render('lab/update', [
            'model' => $model,
            'labels'=> Pack::fhpLabelsErrors($model),
        ]);
    }

    public function actionLabView($id)
    {
        return $this->render('lab/view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionLabDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['lab']);
    }

    public function actionAll($id)
    {

        $searchModelLog = new PackFalseSearch();
        $dataProviderLog = $searchModelLog->search(Yii::$app->request->queryParams);

        $number = Pack::findOne($id)->number;

        $query = PackFalse::find()->where(['number' => $number]);

        $dataProviderLog -> query = $query;
        $dataProviderLog -> sort -> defaultOrder = [
            'date_modify' => SORT_DESC
        ];

        return $this->render('all', [
            'searchModelLog'  => $searchModelLog,
            'dataProviderLog' => $dataProviderLog,
            'model' => $this->findModel($id),
        ]);

    }

}
