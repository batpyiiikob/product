<?php

return [
    'adminEmail' => 'admin@example.com',
    'statusDefault' => 1, //Статус нового продукта
    'statusTuTrue'  => 2, //Статус соответсвия ТУ
    'statusTuFalse' => 3, //Статус не соответсвия ТУ
    'paramTuFalse'  => '<span class="error-fhp">Параметр не соответствует ТУ</span>',
    'paramTuFalseUdel'  => '<span class="error-fhp-udel">Параметр не соответствует ТУ</span>',
];
