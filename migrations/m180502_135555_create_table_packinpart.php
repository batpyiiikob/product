<?php

use yii\db\Migration;

/**
 * Class m180502_135240_create_table_status_part
 */
class m180502_135555_create_table_packinpart extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('packinpart', [
            'id' => $this->primaryKey(),
            'id_part' => $this->integer()->notNull(),
            'id_pack' => $this->integer()->notNull()
        ]);


        // add foreign key
        $this->addForeignKey(
            'fk-packinpart-id_part',
            'packinpart',
            'id_part',
            'part',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-packinpart-id_pack',
            'packinpart',
            'id_pack',
            'pack',
            'id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key
        $this->dropForeignKey(
            'fk-packinpart-id_pack',
            'part'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-packinpart-id_part',
            'part'
        );

        $this->dropTable('packinpart');
    }


}