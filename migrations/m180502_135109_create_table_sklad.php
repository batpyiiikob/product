<?php

use yii\db\Migration;

/**
 * Class m180502_135109_create_table_sklad
 */
class m180502_135109_create_table_sklad extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sklad', [
            'id' => $this->primaryKey(),
            'title' => $this->string(160)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sklad');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180502_135109_create_table_sklad cannot be reverted.\n";

        return false;
    }
    */
}
