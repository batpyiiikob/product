<?php

use yii\db\Migration;

/**
 * Class m180502_135314_create_table_pack_false
 */
class m180502_135314_create_table_pack_false extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pack_false', [
            'id' => $this->primaryKey(),
            'date_create' => $this->dateTime(),
            'date_modify' => $this->dateTime(),
            'number' => $this->string(),
            'id_smena'=> $this->integer(),
            'id_tu'=> $this->integer(),
            'id_marka'=> $this->integer(),
            'id_status'=> $this->integer(),
            'id_sklad'=> $this->integer(),
            'fhp_diam'=> $this->float(),
            'fhp_massdol'=> $this->float(),
            'fhp_nasyp'=> $this->float(),
            'fhp_massdolprokal'=> $this->float(),  //Массовая доля потерь при прокалывании, %
            'fhp_udel_azot'=> $this->float(),
            'fhp_udel_argon'=> $this->float(),
            'fhp_mech'=> $this->float(),
            'fhp_istir'=> $this->float(),
            'fhp_comob' => $this->float(),
            'comment'=> $this->text(),
            'user' => $this->string(20),
        ]);

        // creates index
        $this->createIndex(
            'idx-pack_false-number',
            'pack_false',
            'number'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-pack_false-id_tu',
            'pack_false',
            'id_tu',
            'tu',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-pack_false-id_smena',
            'pack_false',
            'id_smena',
            'smena',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-pack_false-id_marka',
            'pack_false',
            'id_marka',
            'marka',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-pack_false-id_status',
            'pack_false',
            'id_status',
            'status',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-pack_false-id_sklad',
            'pack_false',
            'id_sklad',
            'sklad',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key
        $this->dropForeignKey(
            'fk-pack_false-id_sklad',
            'pack_false'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-pack_false-id_status',
            'pack_false'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-pack_false-id_marka',
            'pack_false'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-pack_false-id_smena',
            'pack_false'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-pack_false-id_tu',
            'pack_false'
        );

        $this->dropTable('pack_false');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180502_135314_create_table_pack_false cannot be reverted.\n";

        return false;
    }
    */
}
