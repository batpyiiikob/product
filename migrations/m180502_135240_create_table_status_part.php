<?php

use yii\db\Migration;

/**
 * Class m180502_135240_create_table_status_part
 */
class m180502_135240_create_table_status_part extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('status_part', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'color' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('status_part');
    }


}
