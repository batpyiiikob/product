<?php

use yii\db\Migration;

/**
 * Class m180502_135151_create_table_fhp_lim
 */
class m180502_135151_create_table_fhp_lim extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('fhp_lim', [
            'id' => $this->primaryKey(),
            'id_marka' => $this->integer(),
            'id_tu' => $this->integer(),
            'diam_min' => $this->float(),
            'diam_max' => $this->float(),
            'massdol_min' => $this->float(),
            'nasyp_min' => $this->float(),
            'nasyp_max' => $this->float(),
            'massdolprokal_max' => $this->float(), //Массовая доля потерь при прокалывании, %
            'udel_min' => $this->float(),
            'mech_min' => $this->float(),
            'istir_max' => $this->float(),
            'comob_min' => $this->float(),
            'comment' => $this->string()
        ]);

        // add foreign key
        $this->addForeignKey(
            'fk-fhp_lim-id_marka',
            'fhp_lim',
            'id_marka',
            'marka',
            'id'
        );

        $this->addForeignKey(
            'fk-fhp_lim-id_tu',
            'fhp_lim',
            'id_tu',
            'tu',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key
        $this->dropForeignKey(
            'fk-fhp_lim-id_marka',
            'fhp_lim'
        );

        $this->dropTable('fhp_lim');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180502_135151_create_table_fhp_lim cannot be reverted.\n";

        return false;
    }
    */
}
