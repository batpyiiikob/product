<?php

use yii\db\Migration;

/**
 * Class m180502_135245_create_table_part_false
 */
class m180502_135245_create_table_part_false extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('part_false', [
            'id' => $this->primaryKey(),
            'date_create' => $this->dateTime(),
            'date_modify' => $this->dateTime(),
            'number' => $this->string()->notNull()->unique(),
            'id_marka' => $this->integer()->notNull(),
            'id_sklad' => $this->integer(),
            'id_partner' => $this->integer(),
            'date_out' => $this->dateTime(),
            'id_status' => $this->integer(),
            'id_tu' => $this->integer(),
            'comment' => $this->string(),
            'user' => $this->string(20),
        ]);

        // creates index
        $this->createIndex(
            'idx-part_false-number',
            'part_false',
            'number'
        );


        // add foreign key
        $this->addForeignKey(
            'fk-part_false-id_marka',
            'part',
            'id_marka',
            'marka',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-part_false-id_status_part',
            'part_false',
            'id_status',
            'status_part',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-part_false-id_sklad',
            'part_false',
            'id_sklad',
            'sklad',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-part_false-id_partner',
            'part_false',
            'id_partner',
            'partner',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-part_false-id_tu',
            'part_false',
            'id_tu',
            'tu',
            'id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        // drops foreign key
        $this->dropForeignKey(
            'fk-part_false-id_partner',
            'part_false'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-part_false-id_sklad',
            'part_false'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-part_false-id_status',
            'part_false'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-part_false-id_marka',
            'part'
        );

        $this->dropTable('part_false');
    }
}
