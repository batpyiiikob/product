<?php

use yii\db\Migration;

/**
 * Class m180502_135111_create_table_smena
 */
class m180502_135111_create_table_smena extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('smena', [
            'id' => $this->primaryKey(),
            'title' => $this->string(160)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('smena');
    }

}

