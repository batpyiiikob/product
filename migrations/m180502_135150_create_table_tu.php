<?php

use yii\db\Migration;

/**
 * Class m180502_135150_create_table_tu
 */
class m180502_135150_create_table_tu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tu', [
            'id' => $this->primaryKey(),
            'title' => $this->string(160)
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tu');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180502_135150_create_table_tu cannot be reverted.\n";

        return false;
    }
    */
}
