<?php

use yii\db\Migration;

/**
 * Class m181008_141600_create_table_data_insert
 */
class m181008_141600_create_table_data_insert extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // Справочник Склад добавить
        $this->insert('sklad', ['title' => 'Склад №2 (цех)']);
        $this->insert('sklad', ['title' => 'Основной склад']);
        $this->insert('sklad', ['title' => 'Склад ГП']);

        // Справочник Состояние добавить
        $this->insert('status', [
            'title' => 'Новый продукт',
            'color' => '#d9ead3'
        ]);

        $this->insert('status', [
            'title' => 'Соответствует ТУ',
            'color' => '#93c47d'
        ]);

        $this->insert('status', [
            'title' => 'Не соответствует ТУ',
            'color' => '#ea9999'
        ]);

        $this->insert('status', [
            'title' => 'На переработку',
            'color' => '#f9cb9c'
        ]);

        $this->insert('status', [
            'title' => 'Переработан',
            'color' => '#cfe2f3'
        ]);

        $this->insert('status', [
            'title' => 'В партии',
            'color' => '#aaaaaa'
        ]);

        $this->insert('status', [
            'title' => 'Реализовано',
            'color' => '#cccccc'
        ]);

        // Справочник Состояние Партий добавить
        $this->insert('status_part', [
            'title' => 'Новая партия',
            'color' => '#d9ead3'
        ]);

          $this->insert('status_part', [
            'title' => 'На согласование',
            'color' => '#cfe2f3'
        ]);

        $this->insert('status_part', [
            'title' => 'Согласовано',
            'color' => '#93c47d'
        ]);

        $this->insert('status_part', [
            'title' => 'Пересмотреть отбор',
            'color' => '#f9cb9c'
        ]);

        $this->insert('status_part', [
            'title' => 'Реализовано',
            'color' => '#cccccc'
        ]);



        // Справочник Марка добавить
        $this->insert('marka', ['title' => 'АО']);
        $this->insert('marka', ['title' => 'АО2']);
        $this->insert('marka', ['title' => 'АН']);
        $this->insert('marka', ['title' => 'АН2']);
        $this->insert('marka', ['title' => 'АК']);
        $this->insert('marka', ['title' => 'АК2']);
        $this->insert('marka', ['title' => 'АС']);
        $this->insert('marka', ['title' => 'АС2']);
        $this->insert('marka', ['title' => 'АД']);
        $this->insert('marka', ['title' => 'ААШГ']);
        $this->insert('marka', ['title' => 'ААШГ-1']);
        $this->insert('marka', ['title' => 'ААШГ-2']);
        $this->insert('marka', ['title' => 'ЗАО-К1']);

        // Справочник Смена добавить
        $this->insert('smena', ['title' => 'А']);
        $this->insert('smena', ['title' => 'Б']);
        $this->insert('smena', ['title' => 'В']);
        $this->insert('smena', ['title' => 'Г']);

        // Справочник ТУ добавить
        $this->insert('tu', ['title' => 'ТУ 2163-143-60201897-2016']);
        $this->insert('tu', ['title' => 'ТУ 2163-142-60201897-2010']);
        $this->insert('tu', ['title' => 'ТУ 20.13.25-145-60201897-2017']);


// ТУ 2163-143-60201897-2016
        // Справочник ФХП АО добавить
        $this->insert('fhp_lim', [
            'id_marka' => '1',
            'id_tu' => '1',
            'diam_min' => 2.8,     //2
            'diam_max' => 8,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.8,    //4
            'massdolprokal_max' => 5, //5
            'udel_min' => 280,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.52,   //9
        ]);

        // Справочник ФХП АО2 добавить
        $this->insert('fhp_lim', [
            'id_marka' => '2',
            'id_tu' => '1',
            'diam_min' => 1.5,     //2
            'diam_max' => 3,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.8,    //4
            'massdolprokal_max' => 5, //5
            'udel_min' => 260,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.5,    //9
        ]);


        // Справочник ФХП АН добавить
        $this->insert('fhp_lim', [
            'id_marka' => '3',
            'id_tu' => '1',
            'diam_min' => 2.8,     //2
            'diam_max' => 8,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.8,    //4
            'massdolprokal_max' => 5, //5
            'udel_min' => 280,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.52,   //9
        ]);

        // Справочник ФХП АН2 добавить
        $this->insert('fhp_lim', [
            'id_marka' => '4',
            'id_tu' => '1',
            'diam_min' => 1.5,     //2
            'diam_max' => 3,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.8,    //4
            'massdolprokal_max' => 5, //5
            'udel_min' => 260,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.5,    //9
        ]);

        // Справочник ФХП АК добавить
        $this->insert('fhp_lim', [
            'id_marka' => '5',
            'id_tu' => '1',
            'diam_min' => 2.8,     //2
            'diam_max' => 8,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.7,    //4
            'massdolprokal_max' => 5, //5
            'udel_min' => 280,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.52,   //9
        ]);

        // Справочник ФХП АК2 добавить
        $this->insert('fhp_lim', [
            'id_marka' => '6',
            'id_tu' => '1',
            'diam_min' => 2.8,     //2
            'diam_max' => 8,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.8,    //4
            'massdolprokal_max' => 5, //5
            'udel_min' => 260,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.5,    //9
        ]);

        // Справочник ФХП АС добавить
        $this->insert('fhp_lim', [
            'id_marka' => '7',
            'id_tu' => '1',
            'diam_min' => 2,       //2
            'diam_max' => 5,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.8,    //4
            'massdolprokal_max' => 5, //5
            'udel_min' => 300,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.52,   //9
        ]);

        // Справочник ФХП АС2 добавить
        $this->insert('fhp_lim', [
            'id_marka' => '8',
            'id_tu' => '1',
            'diam_min' => 2,       //2
            'diam_max' => 5,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.8,    //4
            'massdolprokal_max' => 5, //5
            'udel_min' => 270,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.5,    //9
        ]);

        // Справочник ФХП АД добавить
        $this->insert('fhp_lim', [
            'id_marka' => '9',
            'id_tu' => '1',
            'diam_min' => 5,       //2
            'diam_max' => 12,      //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.8,    //4
            'massdolprokal_max' => 5, //5
            'udel_min' => 240,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.52,    //9
        ]);

        // Справочник ФХП ААШГ добавить
        $this->insert('fhp_lim', [
            'id_marka' => '10',
            'id_tu' => '1',
            'diam_min' => 10,       //2
            'diam_max' => 25,      //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.8,    //4
            'massdolprokal_max' => 5, //5
            'udel_min' => 240,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.5,    //9
        ]);





// ТУ 2163-142-60201897-2010
        // Справочник ФХП АО добавить
        $this->insert('fhp_lim', [
            'id_marka' => '1',
            'id_tu' => '2',
            'diam_min' => 2.8,     //2
            'diam_max' => 8,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.9,    //4
            'massdolprokal_max' => 7, //5
            'udel_min' => 280,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.5,   //9
        ]);

        // Справочник ФХП АН добавить
        $this->insert('fhp_lim', [
            'id_marka' => '3',
            'id_tu' => '2',
            'diam_min' => 2.8,     //2
            'diam_max' => 8,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.9,    //4
            'massdolprokal_max' => 7, //5
            'udel_min' => 280,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.5,   //9
        ]);


        // Справочник ФХП АК добавить
        $this->insert('fhp_lim', [
            'id_marka' => '5',
            'id_tu' => '2',
            'diam_min' => 2.8,     //2
            'diam_max' => 8,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.7,    //4
            'massdolprokal_max' => 7, //5
            'udel_min' => 280,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.5,   //9
        ]);

        // Справочник ФХП АС добавить
        $this->insert('fhp_lim', [
            'id_marka' => '7',
            'id_tu' => '2',
            'diam_min' => 2,       //2
            'diam_max' => 5,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.9,    //4
            'massdolprokal_max' => 7, //5
            'udel_min' => 300,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.5,   //9
        ]);


        // Справочник ФХП АД добавить
        $this->insert('fhp_lim', [
            'id_marka' => '9',
            'id_tu' => '2',
            'diam_min' => 5,       //2
            'diam_max' => 10,      //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.9,    //4
            'massdolprokal_max' => 7, //5
            'udel_min' => 240,     //6
            'mech_min' => 6,       //7
            'istir_max' => 0.5,    //8
            'comob_min' => 0.5,    //9
        ]);


// ТУ 20.13.25-145-60201897-2017
        // Справочник ФХП ААШГ-1 добавить
        $this->insert('fhp_lim', [
            'id_marka' => '11',
            'id_tu' => '3',
            'diam_min' => 20,       //2
            'diam_max' => 35,      //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.6,   //4
            'nasyp_max' => 0.8,    //4
            'massdolprokal_max' => 0, //
            'udel_min' => 250,     //6
            'mech_min' => 0,       //7
            'istir_max' => 0,    //8
            'comob_min' => 0,    //9
        ]);

        // Справочник ФХП ААШГ-2 добавить
        $this->insert('fhp_lim', [
            'id_marka' => '12',
            'id_tu' => '3',
            'diam_min' => 12,       //2
            'diam_max' => 20,      //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.6,   //4
            'nasyp_max' => 0.8,    //4
            'massdolprokal_max' => 0, //5
            'udel_min' => 250,     //6
            'mech_min' => 0,       //7
            'istir_max' => 0,    //8
            'comob_min' => 0,    //9
        ]);

        // Справочник ФХП АК добавить
        $this->insert('fhp_lim', [
            'id_marka' => '5',
            'id_tu' => '3',
            'diam_min' => 4,     //2
            'diam_max' => 8,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.6,   //4
            'nasyp_max' => 0.8,    //4
            'massdolprokal_max' => 6, //5
            'udel_min' => 300,     //6
            'mech_min' => 5,       //7
            'istir_max' => 0,    //8
            'comob_min' => 0.5,   //9
        ]);

        // Справочник ФХП АД добавить
        $this->insert('fhp_lim', [
            'id_marka' => '9',
            'id_tu' => '3',
            'diam_min' => 8,       //2
            'diam_max' => 12,      //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.6,   //4
            'nasyp_max' => 0.9,    //4
            'massdolprokal_max' => 6, //5
            'udel_min' => 250,     //6
            'mech_min' => 5,       //7
            'istir_max' => 0,    //8
            'comob_min' => 0.5,    //9
        ]);

        // Справочник ФХП АО добавить
        $this->insert('fhp_lim', [
            'id_marka' => '1',
            'id_tu' => '3',
            'diam_min' => 2.8,     //2
            'diam_max' => 8,       //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.65,   //4
            'nasyp_max' => 0.9,    //4
            'massdolprokal_max' => 6, //5
            'udel_min' => 300,     //6
            'mech_min' => 5,       //7
            'istir_max' => 0,    //8
            'comob_min' => 0.5,   //9
        ]);

        // Справочник ФХП ЗАО-К1 добавить
        $this->insert('fhp_lim', [
            'id_marka' => '13',
            'id_tu' => '3',
            'diam_min' => 2.8,       //2
            'diam_max' => 8,      //2
            'massdol_min' => 96,   //3
            'nasyp_min' => 0.6,   //4
            'nasyp_max' => 1,    //4
            'massdolprokal_max' => 9, //5
            'udel_min' => 250,     //6
            'mech_min' => 5,       //7
            'istir_max' => 0,    //8
            'comob_min' => 0.4,    //9
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

}
