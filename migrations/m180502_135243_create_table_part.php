<?php

use yii\db\Migration;

/**
 * Class m180502_135243_create_table_part
 */
class m180502_135243_create_table_part extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('part', [
            'id' => $this->primaryKey(),
            'date_create' => $this->dateTime(),
            'date_modify' => $this->dateTime(),
            'number' => $this->string()->notNull()->unique(),
            'id_marka' => $this->integer()->notNull(),
            'id_sklad' => $this->integer(),
            'id_partner' => $this->integer(),
            'date_out' => $this->dateTime(),
            'id_status' => $this->integer(),
            'id_tu' => $this->integer(),
            'comment' => $this->string()
        ]);

        // creates index
        $this->createIndex(
            'idx-part-number',
            'part',
            'number'
        );


        // add foreign key
        $this->addForeignKey(
            'fk-part-id_marka',
            'part',
            'id_marka',
            'marka',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-part-id_status_part',
            'part',
            'id_status',
            'status_part',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-part-id_sklad',
            'part',
            'id_sklad',
            'sklad',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-part-id_partner',
            'part',
            'id_partner',
            'partner',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-part-id_tu',
            'part',
            'id_tu',
            'tu',
            'id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        // drops foreign key
        $this->dropForeignKey(
            'fk-part-id_partner',
            'part'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-part-id_sklad',
            'part'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-part-id_status',
            'part'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-part-id_marka',
            'part'
        );

        $this->dropTable('part');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180502_135243_create_table_part cannot be reverted.\n";

        return false;
    }
    */
}
