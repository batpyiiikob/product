<?php

use yii\db\Migration;

/**
 * Class m180502_135248_create_table_pack
 */
class m180502_135248_create_table_pack extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pack', [
            'id' => $this->primaryKey(),
            'date_create' => $this->dateTime(),
            'date_modify' => $this->dateTime(),
            'number' => $this->string()->notNull()->unique(),
            'id_smena'=> $this->integer(),
            'id_tu'=> $this->integer(),
            'id_marka'=> $this->integer(),
            'id_status'=> $this->integer(),
            'id_sklad'=> $this->integer(),
            'fhp_diam'=> $this->float(),        //Диаметр гранул, мм
            'fhp_massdol'=> $this->float(),     //Массовая доля гранул заданного диаметра, %
            'fhp_nasyp'=> $this->float(),       //Насыпная плотность, г/см3
            'fhp_massdolprokal'=> $this->float(), //Массовая доля потерь при прокалывании, %
            'fhp_udel_azot'=> $this->float(),   //Удельная поверхность по АЗОТУ, м2/г
            'fhp_udel_argon'=> $this->float(),  // Удельная поверхность по АРГОНУ, м2/г
            'fhp_mech'=> $this->float(),        // Механическая прочность при раздавливании
            'fhp_istir'=> $this->float(),       // Истираемость поверхности слоя
            'fhp_comob' => $this->float(),      // Общий объём пор, см3/г
            'comment'=> $this->text(),
            //'id_part'=> $this->integer()
        ]);

        // creates index
        $this->createIndex(
            'idx-pack-number',
            'pack',
            'number'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-pack-id_tu',
            'pack',
            'id_tu',
            'tu',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-pack-id_smena',
            'pack',
            'id_smena',
            'smena',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-pack-id_marka',
            'pack',
            'id_marka',
            'marka',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-pack-id_status',
            'pack',
            'id_status',
            'status',
            'id'
        );

        // add foreign key
        $this->addForeignKey(
            'fk-pack-id_sklad',
            'pack',
            'id_sklad',
            'sklad',
            'id'
        );

        // add foreign key
 /*       $this->addForeignKey(
            'fk-pack-id_part',
            'pack',
            'id_part',
            'part',
            'id'
        );*/
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key
 /*       $this->dropForeignKey(
            'fk-pack-id_part',
            'pack'
        );*/

        // drops foreign key
        $this->dropForeignKey(
            'fk-pack-id_sklad',
            'pack'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-pack-id_status',
            'pack'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-pack-id_marka',
            'pack'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-pack-id_smena',
            'pack'
        );

        // drops foreign key
        $this->dropForeignKey(
            'fk-pack-id_tu',
            'pack'
        );

        $this->dropTable('pack');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180502_135248_create_table_pack cannot be reverted.\n";

        return false;
    }
    */
}
