<?php

use yii\db\Migration;

/**
 * Class m180502_135240_create_table_partner
 */
class m180502_135240_create_table_partner extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('partner', [
            'id' => $this->primaryKey(),
            'title' => $this->string(160),
            'adress' => $this->string(),
            'phone' => $this->string()
        ]);

        // creates index
        $this->createIndex(
            'idx-partner-title',
            'partner',
            'title'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('partner');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180502_135240_create_table_partner cannot be reverted.\n";

        return false;
    }
    */
}
