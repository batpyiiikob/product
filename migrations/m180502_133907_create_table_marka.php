<?php

use yii\db\Migration;

/**
 * Class m180502_133907_create_table_marka
 */
class m180502_133907_create_table_marka extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('marka', [
            'id' => $this->primaryKey(),
            'title' => $this->string(160)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('marka');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180502_133907_create_table_marka cannot be reverted.\n";

        return false;
    }
    */
}
