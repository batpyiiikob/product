<?php

use yii\db\Migration;

/**
 * Class m180502_134907_create_table_status
 */
class m180502_134907_create_table_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('status', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'color' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('status');
    }


}
