<?php

// https://toster.ru/q/520790
// http://demos.krajee.com/widget-details/depdrop
// https://toster.ru/q/535699#answer_1227415


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Part */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="part-form">

    <div class="row">

        <div class="col-md-4">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'date_create')->textInput(array(
                    'value' => (new \DateTime('now', new \DateTimeZone('Europe/Moscow')))->format('Y-m-d H:i:s'),
                    'class'=>'form-control','readonly' => true));
                ?>

                <?= $form->field($model, 'id_marka')
                        ->dropDownList(\app\models\Marka::find()
                            ->select(['title','id'])
                            ->indexBy('id')
                            ->column(),
                            array("disabled"=>"disabled")
                        )
                ?>

                <?= $form->field($model, 'number')->textInput(array(
                    'readonly' => true,
                    'value' => $model->number));
                ?>


                <?= $form->field($model, 'id_sklad')->dropDownList(\app\models\Sklad::find()->select(['title','id'])->indexBy('id')->column() ) ?>

                <?= $form->field($model, 'id_partner')->dropDownList(\app\models\Partner::find()->select(['title','id'])->indexBy('id')->column() ) ?>


                <?= $form->field($model, 'date_out')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Ввод даты/времени...'],
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'weekStart'=>1, //неделя начинается с понедельника
                        'startDate' => '01-01-2018 00:00', //самая ранняя возможная дата
                        'todayBtn'=>true, //снизу кнопка "сегодня"
                    ]
                ]);
                ?>

                <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>


        </div>

        <div class="col-md-4">
            <h3>Продукция в партии</h3>
        </div>

        <div class="col-md-4">
            <h3>Продукция для отбора</h3>

        </div>

    </div>



    </div>
