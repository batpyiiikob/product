<?php


use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PartSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="part-index">

    <div class="alert alert-info">
        <h1>Все партии</h1>
    </div>

    <?  $dataColumns =
        [
            //'id',
            'date_create',
            'number',
            [
                'attribute'=>'id_marka',
                'value'=>'marka.title',
                'filter' => Html::activeDropDownList($searchModel, 'id_marka',
                    ArrayHelper::map(\app\models\Marka::find()->asArray()->all(), 'id', 'title'),
                    ['class'=>'form-control', 'prompt' => '...']),
                'enableSorting' => true,
            ],


            [
                'attribute'=>'id_partner',
                'value'=>'partner.title',
                'filter' => Html::activeDropDownList($searchModel, 'id_partner',
                    ArrayHelper::map(\app\models\Partner::find()->asArray()->all(), 'id', 'title'),
                    ['class'=>'form-control', 'prompt' => '...']),
                'enableSorting' => true,
            ],

            'date_out',


            [
                'attribute'=>'id_status',
                'value'=>'status.title',
                'filter' => Html::activeDropDownList($searchModel, 'id_status',
                    ArrayHelper::map(\app\models\StatusPart::find()->asArray()->all(), 'id', 'title'),
                    ['class'=>'form-control', 'prompt' => '...']),
                'enableSorting' => true,

                'contentOptions' =>function ($model, $key, $index, $column) {
                    $clr = \app\models\StatusPart::findOne($model->id_status)->color;
                    return ['style' => 'background-color:'.$clr];
                },
            ],



            'comment',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],

                'template' => '{view}',

            ],


        ]
    ?>




    <?= GridView::widget([
        'dataProvider'=>$dataProvider,
        'rowOptions' => ['style' => 'background-color:white;'],
        'pjax'=>true,
        'responsive'=>true,
        'panel'=>['type'=>'primary'],

        'columns' => $dataColumns,

        'toolbar' =>  [
            '{export}',
            '{toggleData}'
        ],
        'exportConfig' => [
            GridView::EXCEL => ['label' => 'Сохранить в файл EXCEL'],
        ],
        'filterModel'=>$searchModel,
    ]); ?>




</div>
