<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Part */

$this->title = 'Новая партия';
$this->params['breadcrumbs'][] = ['label' => 'Партии', 'url' => ['/part/lab']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="part-create">
    <div class="alert alert-info">
        <h1>ЛАБОРАТОРИЯ</h1>
    </div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
