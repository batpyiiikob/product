<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Part */

$this->title = 'Изменение параметров партии';
$this->params['breadcrumbs'][] = ['label' => 'Партии', 'url' => ['/part/lab']];
$this->params['breadcrumbs'][] = ['label' => $model->number, 'url' => ['/part/lab-view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="part-update">

    <div class="alert alert-info">
        <h1>ЛАБОРАТОРИЯ</h1>
    </div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formupdate', [
        'dataProvider' => $dataProvider,
        'model' => $model,
    ]) ?>

</div>
