<?php

// https://toster.ru/q/520790
// http://demos.krajee.com/widget-details/depdrop
// https://toster.ru/q/535699#answer_1227415


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Part */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="part-form">

    <div class="row">

        <div class="col-md-4">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'date_create')->textInput(array(
                'value' => (new \DateTime('now', new \DateTimeZone('Europe/Moscow')))->format('Y-m-d H:i:s'),
                'class'=>'form-control','readonly' => true));
            ?>


            <?= $form->field($model, 'number')->textInput(array(
                'maxlength' => true,
                'readonly' => true,
                'class'=>'form-control part-number'));
            ?>

            <?= $form->field($model, 'id_status')
                ->dropDownList(\app\models\StatusPart::find()->select(['title','id'])->where(['id' => [2]])
                    ->indexBy('id')->column())
            ?>

            <?= $form->field($model, 'comment')->textarea(['rows' => 4]) ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>





        <div class="col-md-8">
            <h3>Продукция в партии</h3>

            <?php Pjax::begin(['id' => 'pjax-container-packinpart', 'enablePushState' => false])?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'numberPart',
                    'numberPack',
                    'dataCreatedPack',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],

                        'template' => '{view}',

                        'buttons' => [

                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => 'Подробнее',
                                ]);
                            },

                        ],

                        'urlCreator' => function ($action, $model, $key, $index) {

                            if ($action === 'view') {
                                $url ='index.php?r=pack%2Fklad-view&id='.$model->id_pack;
                                return $url;
                            };

                        },

                    ],

                ],

            ]); ?>
            <?php Pjax::end(); ?>

        </div>




    </div>

</div>

