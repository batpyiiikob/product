<?php

// https://toster.ru/q/520790
// http://demos.krajee.com/widget-details/depdrop
// https://toster.ru/q/535699#answer_1227415


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model app\models\Part */
/* @var $form yii\widgets\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>




<div class="part-form">

    <div class="row">

        <div class="col-md-4">

                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'date_create')->textInput(array(
                    'value' => (new \DateTime('now', new \DateTimeZone('Europe/Moscow')))->format('Y-m-d H:i:s'),
                    'class'=>'form-control','readonly' => true));
                ?>

                <?= $form->field($model, 'id_tu')
                            ->dropDownList(\app\models\Tu::find()
                                ->select(['title','id'])
                                ->indexBy('id')
                                ->column(),
                                [
                                    'prompt' => '',
                                ]
                            )
                ?>

                <?= $form->field($model, 'id_marka')
                            ->dropDownList(\app\models\Marka::find()
                                ->select(['title','id'])
                                ->indexBy('id')
                                ->column(),
                                [
                                    'prompt' => '',
                                    'onchange' => '$.post(
                                                  "'.Url::toRoute('part/num-part').'",
                                                  {id : $(this).val()},
                                                  function(data){
                                                      $(".part-number").val(data)
                                                  }
                                    )'
                                ]
                            )
                ?>

                <?= $form->field($model, 'number')->textInput(array(
                        'maxlength' => true,
                        'readonly' => true,
                        'value' => 'Выберите марку...',
                        'class'=>'form-control part-number'));
                ?>

                <?= $form->field($model, 'id_status')
                    ->dropDownList(\app\models\StatusPart::find()->select(['title','id'])->where(['id' => [1]])
                        ->indexBy('id')->column()) ?>

                <div style = 'display:none' >
                    <?= $form->field($model, 'id_sklad')
                        ->dropDownList(\app\models\Sklad::find()
                            ->select(['title','id'])
                            ->indexBy('id')
                            ->column(),
                            [
                                'options'=>['2'=>['selected'=>true]]
                            ]
                        )
                    ?>
                </div>

                <?= $form->field($model, 'comment')->textarea(['rows' => 4]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

        </div>





    </div>



    </div>

