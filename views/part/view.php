<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Part */

$this->title = $model->number;
$this->params['breadcrumbs'][] = ['label' => 'Партии', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="part-view">

    <h1><?= 'Партия № '.Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'date_create',
            'number',

            [
                'attribute' => 'marka.title',
            ],


            [
                'attribute' => 'partner.title',
            ],

            'date_out',

            [
                'attribute' => 'status.title',
            ],

            'comment',
        ],
    ]) ?>

</div>




<div>
    <h3>Продукция в партии</h3>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'numberPart',
            'numberPack',
            'dataCreatedPack',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],

                'template' => '{view}',

                'buttons' => [

                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => 'Подробнее',
                        ]);
                    },

                ],

                'urlCreator' => function ($action, $model, $key, $index) {

                    if ($action === 'view') {
                        $url ='index.php?r=pack%2Fview&id='.$model->id_pack;
                        return $url;
                    };

                },

            ],

        ],

    ]); ?>

</div>