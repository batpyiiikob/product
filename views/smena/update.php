<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Smena */

$this->title = 'Редактировать наименование смены';
$this->params['breadcrumbs'][] = ['label' => 'Смены', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="smena-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
