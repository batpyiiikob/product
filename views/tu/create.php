<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tu */

$this->title = 'Добавить ТУ';
$this->params['breadcrumbs'][] = ['label' => 'ТУ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tu-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
