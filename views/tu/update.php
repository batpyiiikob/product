<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tu */

$this->title = 'Редактировать ТУ';
$this->params['breadcrumbs'][] = ['label' => 'ТУ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="tu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
