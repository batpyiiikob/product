<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Packinpart */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="packinpart-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_part')->textInput() ?>

    <?= $form->field($model, 'id_pack')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
