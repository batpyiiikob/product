<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукция в партии: ' . $partNumber;

$this->params['breadcrumbs'][] = ['label' => 'Партии', 'url' => ['/part/lab']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="packinpart-index">

        <div class="jumbotron jumbotron-fluid">
            <h3>Продукция в партии <?= $partNumber ?></h3>
        </div>


        <?php Pjax::begin(['id' => 'pjax-container-packinpart', 'enablePushState' => false])?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id_part',
                //'numberPart',

                'numberPack',
                'dataCreatedPack',

                [
                    'class'    => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'buttons'  => [
                        'delete' => function ($url) {
                            return Html::a(Yii::t('yii', 'Delete'), '#', [
                                'title' => Yii::t('yii', 'Delete'),
                                'aria-label' => Yii::t('yii', 'Delete'),
                                'onclick' => "
                                if (confirm('Удалить продукт из партии?')) {
                                
                                    $.ajax('$url', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        //$.pjax.reload({container: '#pjax-container-packinpart'});
                                        
                                        var pjaxContainers = ['#pjax-container-packinpart', '#pjax-container-pack'];
                                        $.pjax.reload({container: pjaxContainers[0]});
                                        $.each(pjaxContainers , function(index, container) { 
                                             if (index+1 < pjaxContainers.length) {
                                                  $(container).one('pjax:end', function (xhr, options) {
                                                       $.pjax.reload({container: pjaxContainers[index+1]}) ;
                                                  });
                                             }
                                        });
                                        
                                    });
                                    
                                }
                                return false;
                            ",
                            ]);
                        },
                    ],
                ],


                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],

                    'template' => '{view}',

                    'buttons' => [

                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => 'Подробнее',
                            ]);
                        },
                    ],

                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url ='index.php?r=pack%2Flab-view&id='.$model->id_pack;
                            return $url;
                        };
                    },

                ],








            ],

        ]); ?>
        <?php Pjax::end(); ?>

</div>





<div class="jumbotron jumbotron-fluid">
    <h3>Продукция для отбора</h3>

    <button id="addpack" type="button" class="btn btn-success">Добавить выбранные в партию</button>

</div>


<?php Pjax::begin(['id' => 'pjax-container-pack']) ?>
<?= GridView::widget([
    'id' => 'grid-pack',
    'dataProvider' => $dataProviderPacks,
    'filterModel' => $searchPackModel,

    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'date_create',
        'number',

        [
            'attribute'=>'id_status',
            'value'=>'status.title',
            'filter' => Html::activeDropDownList($searchPackModel, 'id_status',
                ArrayHelper::map(\app\models\Status::find()->where(['id' => [2,3]])->asArray()->all(), 'id', 'title'),
                ['class'=>'form-control', 'prompt' => '...']),
            'enableSorting' => true,
            'contentOptions' =>function ($modelPack, $key, $index, $column) {
                $clr = \app\models\Status::findOne($modelPack->id_status)->color;
                return ['style' => 'background-color:'.$clr];
            },
        ],
        [
            'class' => 'yii\grid\CheckboxColumn',
            'checkboxOptions' => function ($model, $key, $index, $column) {
                return ['value' => $model->id];
            }
        ],
    ],
]); ?>
<?php Pjax::end(); ?>


</div>



<?php
$js = <<<JS

    $("#addpack").on("click", function(){
        
        var keys = $('#grid-pack').yiiGridView('getSelectedRows');
        
        $.ajax({
            url: 'index.php?r=packinpart/change',
            type: 'POST',
            data: {keys : keys, id: $partID},
            success: function(res){
                //$('.jumbotron h2').text(res);
                var pjaxContainers = ['#pjax-container-packinpart', '#pjax-container-pack'];
                $.pjax.reload({container: pjaxContainers[0]});
                $.each(pjaxContainers , function(index, container) { 
                     if (index+1 < pjaxContainers.length) {
                          $(container).one('pjax:end', function (xhr, options) {
                               $.pjax.reload({container: pjaxContainers[index+1]}) ;
                          });
                     }
                });
        },
        error: function(){
            alert('Ничего не выбрано!');
        }
        });
        return false;
    });

JS;

$this->registerJs($js);
?>

