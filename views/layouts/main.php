<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);



    if (Yii::$app->user->isGuest){
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [

                ['label' => 'Все партии', 'url' => ['/part/index']],
                ['label' => 'Все продукты', 'url' => ['/pack/index']],

                Yii::$app->user->isGuest ? (
                ['label' => 'Вход', 'url' => ['/site/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Выход (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);
    }


    if (Yii::$app->user->id == 101){
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Сменщик', 'url' => ['/pack/smena']],

                Yii::$app->user->isGuest ? (
                ['label' => 'Вход', 'url' => ['/site/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Выход (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);
    }


    if (Yii::$app->user->id == 102){
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Мешки', 'url' => ['/pack/klad']],
                ['label' => 'Партии', 'url' => ['/part/klad']],
                ['label' => 'Партнёры', 'url' => ['/partner/index']],

                Yii::$app->user->isGuest ? (
                ['label' => 'Вход', 'url' => ['/site/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Выход (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);
    }

    if (Yii::$app->user->id == 103){
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Мешки', 'url' => ['/pack/lab']],
                ['label' => 'Партии', 'url' => ['/part/lab']],

                Yii::$app->user->isGuest ? (
                ['label' => 'Вход', 'url' => ['/site/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Выход (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);
    }

    if (Yii::$app->user->id == 100){
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Сменщик', 'url' => ['/pack/smena']],

            //['label' => 'Кладовщик', 'url' => ['/pack/klad']],

            ['label' => 'Кладовщик','options' => ['id' => 'down_lab'], 'items'=>[

                ['label' => 'Мешки', 'url' => ['/pack/klad']],
                ['label' => 'Партии', 'url' => ['/part/klad']],

            ]],
            //['label' => 'Лаборатория', 'url' => ['/pack/lab']],

            ['label' => 'Лаборатория','options' => ['id' => 'down_lab'], 'items'=>[

                ['label' => 'Мешки', 'url' => ['/pack/lab']],
                ['label' => 'Партии', 'url' => ['/part/lab']],

            ]],

            ['label' => 'Справочники','options' => ['id' => 'down_spravochniki'], 'items'=>[

                ['label' => 'Склады', 'url' => ['/sklad/index']],
                ['label' => 'Марка', 'url' => ['/marka/index']],
                ['label' => 'Смены', 'url' => ['/smena/index']],
                ['label' => 'Статусы продукции', 'url' => ['/status/index']],
                ['label' => 'Статусы партий', 'url' => ['/status-part/index']],
                [
                    'label' => '',
                    'options' => [
                        'role' => 'presentation',
                        'class' => 'divider'
                    ]
                ],
                ['label' => 'ТУ', 'url' => ['/tu/index']],
                ['label' => 'ФХП', 'url' => ['/fhp-lim/index']],
                [
                    'label' => '',
                    'options' => [
                        'role' => 'presentation',
                        'class' => 'divider'
                    ]
                ],
                ['label' => 'Партнёры', 'url' => ['/partner/index']],
            ]],

            ['label' => 'Все партии', 'url' => ['/part/index']],
            ['label' => 'Все продукты', 'url' => ['/pack/index']],


            Yii::$app->user->isGuest ? (
                ['label' => 'Вход', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Выход (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    }

    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; ООО "НКЗ" <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
