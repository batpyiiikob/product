<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Marka */

$this->title = 'Добавить марку';
$this->params['breadcrumbs'][] = ['label' => 'Марки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marka-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
