<?php

/* @var $this yii\web\View */


use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

$this->title = 'Учёт готовой продукции';
?>
<div class="site-index">

    <div class="body-content">
        <div class="alert alert-info">
            <h1>Готовая продукция (мешки)</h1>
        </div>
                <?  $dataPackColumns =
                    [
                        [
                            'attribute' => 'date_create',
                            'format' =>  ['date', 'yyyy.MM.dd H:m:s'],
                            'options' => ['width' => '200']
                        ],

                        'number',

                        [
                            'attribute'=>'id_marka',
                            'value'=>'marka.title',
                            'filter' => Html::activeDropDownList($searchPackModel, 'id_marka',
                                ArrayHelper::map(\app\models\Marka::find()->asArray()->all(), 'id', 'title'),
                                ['class'=>'form-control', 'prompt' => '...']),
                            'enableSorting' => true,
                        ],


                        [
                            'attribute'=>'id_sklad',
                            'value'=>'sklad.title',
                            'filter' => Html::activeDropDownList($searchPackModel, 'id_sklad',
                                ArrayHelper::map(\app\models\Sklad::find()->asArray()->all(), 'id', 'title'),
                                ['class'=>'form-control', 'prompt' => '...']),
                            'enableSorting' => true,
                        ],

                        [
                            'attribute'=>'id_status',
                            'value'=>'status.title',
                            'filter' => Html::activeDropDownList($searchPackModel, 'id_status',
                                ArrayHelper::map(\app\models\Status::find()->asArray()->all(), 'id', 'title'),
                                ['class'=>'form-control', 'prompt' => '...']),
                            'enableSorting' => true,

                            'contentOptions' =>function ($model, $key, $index, $column) {
                                    $clr = \app\models\Status::findOne($model->id_status)->color;
                                    return ['style' => 'background-color:'.$clr];
                            },
                        ],

                        'comment:ntext',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],

                            'template' => '{view} {all}',

                            'buttons' => [

                                'view' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                        'title' => 'Подробнее',
                                    ]);
                                },

                                'all' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-th-list"></span>', $url, [
                                        'title' => 'История продукта',
                                    ]);
                                },

                            ],

                            'urlCreator' => function ($action, $model, $key, $index) {

                                if ($action === 'view') {
                                    $url ='index.php?r=pack%2Fview&id='.$model->id;
                                    return $url;
                                };

                                if ($action === 'all') {
                                    $url ='index.php?r=pack%2Fall&id='.$model->id;
                                    return $url;
                                };

                            },

                        ],
                    ]
                ?>

                <?= GridView::widget([
                    'dataProvider'=>$dataPackProvider,
                    'rowOptions' => ['style' => 'background-color:white;'],
                    'pjax'=>false,
                    'responsive'=>true,
                    'panel'=>['type'=>'primary'],

                    'columns' => $dataPackColumns,

                    'toolbar' =>  [
                        '{export}',
                        '{toggleData}'
                    ],
                    'exportConfig' => [
                        GridView::EXCEL => ['label' => 'Сохранить в файл EXCEL'],
                    ],
                    'filterModel'=>$searchPackModel,
                ]); ?>

            </div>



    <div class="alert alert-info">
        <h1>Партии готовой продукции</h1>
    </div>
            <?  $dataPartColumns =
                [
                    'date_create',
                    'number',
                    [
                        'attribute'=>'id_marka',
                        'value'=>'marka.title',
                        'filter' => Html::activeDropDownList($searchPartModel, 'id_marka',
                            ArrayHelper::map(\app\models\Marka::find()->asArray()->all(), 'id', 'title'),
                            ['class'=>'form-control', 'prompt' => '...']),
                        'enableSorting' => true,
                    ],

                    [
                        'attribute'=>'id_sklad',
                        'value'=>'sklad.title',
                        'filter' => Html::activeDropDownList($searchPartModel, 'id_sklad',
                            ArrayHelper::map(\app\models\Sklad::find()->asArray()->all(), 'id', 'title'),
                            ['class'=>'form-control', 'prompt' => '...']),
                        'enableSorting' => true,
                    ],

                    [
                        'attribute'=>'id_partner',
                        'value'=>'partner.title',
                        'filter' => Html::activeDropDownList($searchPartModel, 'id_partner',
                            ArrayHelper::map(\app\models\Partner::find()->asArray()->all(), 'id', 'title'),
                            ['class'=>'form-control', 'prompt' => '...']),
                        'enableSorting' => true,
                    ],

                    'date_out',
                    [
                        'attribute'=>'id_status',
                        'value'=>'status.title',
                        'filter' => Html::activeDropDownList($searchPackModel, 'id_status',
                            ArrayHelper::map(\app\models\StatusPart::find()->asArray()->all(), 'id', 'title'),
                            ['class'=>'form-control', 'prompt' => '...']),
                        'enableSorting' => true,

                        'contentOptions' =>function ($model, $key, $index, $column) {
                            $clr = \app\models\StatusPart::findOne($model->id_status)->color;
                            return ['style' => 'background-color:'.$clr];
                        },
                    ],

                    'comment',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],

                        'template' => '{view}',

                        'buttons' => [

                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => 'Подробнее',
                                ]);
                            },
                        ],

                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'view') {
                                $url ='index.php?r=part%2Fview&id='.$model->id;
                                return $url;
                            };
                        },

                    ],


                ]
            ?>




            <?= GridView::widget([
                'dataProvider'=>$dataPartProvider,
                'rowOptions' => ['style' => 'background-color:white;'],
                'pjax'=>true,
                'responsive'=>true,
                'panel'=>['type'=>'primary'],

                'columns' => $dataPartColumns,

                'toolbar' =>  [
                    '{export}',
                    '{toggleData}'
                ],
                'exportConfig' => [
                    GridView::EXCEL => ['label' => 'Сохранить в файл EXCEL'],
                ],
                'filterModel'=>$searchPartModel,
            ]); ?>


</div>
