<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FhpLim */

$this->title = $model->tu->title.' (Марка: '.$model->marka->title . ')';
$this->params['breadcrumbs'][] = ['label' => 'Физико-Химические показатели', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fhp-lim-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить запись?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'marka.title',
            'tu.title',
            'diam_min',
            'diam_max',
            'massdol_min',
            'nasyp_min',
            'nasyp_max',
            'massdolprokal_max',
            'udel_min',
            'mech_min',
            'istir_max',
            'comob_min',
            'comment',
        ],
    ]) ?>

</div>
