<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FhpLim */

$this->title = 'Добавить Физико-Химические показатели';
$this->params['breadcrumbs'][] = ['label' => 'Физико-Химические показатели', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fhp-lim-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
