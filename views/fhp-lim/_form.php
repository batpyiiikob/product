<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FhpLim */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fhp-lim-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12">
    <?= $form->field($model, 'id_tu')->dropDownList(\app\models\Tu::find()->select(['title','id'])->indexBy('id')->column() ) ?>
    </div>

    <div class="col-md-12">
    <?= $form->field($model, 'id_marka')->dropDownList(\app\models\Marka::find()->select(['title','id'])->indexBy('id')->column() ) ?>
    </div>

    <div class="col-md-6">
    <?= $form->field($model, 'diam_min')->textInput() ?>
    </div>
    <div class="col-md-6">
    <?= $form->field($model, 'diam_max')->textInput() ?>
    </div>
    <div class="col-md-12">
    <?= $form->field($model, 'massdol_min')->textInput() ?>
    </div>
    <div class="col-md-6">
    <?= $form->field($model, 'nasyp_min')->textInput() ?>
    </div>
    <div class="col-md-6">
    <?= $form->field($model, 'nasyp_max')->textInput() ?>
    </div>


<div class="col-md-12">
    <?= $form->field($model, 'massdolprokal_max')->textInput() ?>
</div>

<div class="col-md-12">
    <?= $form->field($model, 'udel_min')->textInput() ?>
</div>

<div class="col-md-12">
    <?= $form->field($model, 'mech_min')->textInput() ?>
</div>

<div class="col-md-12">
    <?= $form->field($model, 'istir_max')->textInput() ?>
</div>

<div class="col-md-12">
    <?= $form->field($model, 'comob_min')->textInput() ?>
</div>

<div class="col-md-12">
    <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="form-group col-md-12">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
