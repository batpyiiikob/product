<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FhpLim */

$this->title = 'Редактировать Физико-Химические показатели';
$this->params['breadcrumbs'][] = ['label' => 'Физико-Химические показатели', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tu->title.' (Марка: '.$model->marka->title . ')', 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="fhp-lim-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
