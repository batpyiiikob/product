<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Физико-Химические показатели';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fhp-lim-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить Физико-Химические показатели', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'id_tu',
                'value'=>'tu.title',
                'enableSorting' => true,
            ],

            [
                'attribute'=>'id_marka',
                'value'=>'marka.title',
                'enableSorting' => true,
            ],


            //'diam_min',
            //'diam_max',
            //'massdol_min',
            //'nasyp_min',
            //'nasyp_max',
            //'massdolprokal_max',
            //'udel_min',
            //'mech_min',
            //'istir_max',
            //'comob_min',
            'comment',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
