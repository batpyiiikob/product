<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\StatusPart */

$this->title = 'Create Status Part';
$this->params['breadcrumbs'][] = ['label' => 'Status Parts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-part-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
