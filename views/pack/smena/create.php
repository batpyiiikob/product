<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pack */

$this->title = 'Новый мешок';
$this->params['breadcrumbs'][] = ['label' => 'Мешки', 'url' => ['smena']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pack-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
