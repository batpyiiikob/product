<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pack */

$this->title = 'Редактировать продукт: '.$model->number;
$this->params['breadcrumbs'][] = ['label' => 'Мешки', 'url' => ['smena']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pack-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <h2><?= $model->tu->title; ?></h2><br>

    <?= $this->render('_formupdate', [
        'model' => $model,
    ]) ?>

</div>
