<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\widgets\MaskedInput;


/* @var $this yii\web\View */
/* @var $model app\models\Pack */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pack-form">
    <strong class = "lab_update_status" style="background-color: <?= $model->status->color; ?>">Текущий статус: <?= $model->status->title; ?></strong><br><br>
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-md-6">

            <?= $form->field($model, 'date_create')->textInput(array('class'=>'form-control',
                                     'readonly' => true));
            ?>

            <?= $form->field($model, 'date_modify')->textInput(array(
                'value' => (new \DateTime('now', new \DateTimeZone('Europe/Moscow')))->format('Y-m-d H:i:s'),
                'class'=>'form-control',
                'readonly' => true));
            ?>

            <?= $form->field($model, 'number')->textInput(array(
                    'readonly' => true,
                    'maxlength' => true,
                    'value' => $model->number,
                    'class'=>'form-control'));
            ?>

        </div>

        <div class="col-md-6">

            <? /* = $form->field($model, 'id_marka')->dropDownList(\app\models\Marka::find()->select(['title','id'])->indexBy('id')->column() ) */ ?>

            <?= $form->field($model, 'id_smena')->dropDownList(\app\models\Smena::find()->select(['title','id'])->indexBy('id')->column() ) ?>

            <div style = 'display:none' >
                <?= $form->field($model, 'id_status')->dropDownList(\app\models\Status::find()->select(['title','id'])->where(['id' => [5]])->indexBy('id')->column() ) ?>
            </div>


        </div>

    </div>

    <?= $form->field($model, 'comment')->textarea(['rows' => 2]) ?>

    <div class="form-group">
        <?= Html::submitButton('Подтвердить статус', ['class' => 'btn btn-success']) ?>
        <strong class = "lab_update_status lab_new_status">Новый статус: Переработан</strong>
    </div>

    <?php ActiveForm::end(); ?>

</div>
