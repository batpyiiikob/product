<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мешки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pack-index">

    <div class="alert alert-success">
        <h1>СМЕНА</h1>
    </div>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Новый мешок', ['smena-create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?  $dataColumns =
        [
            [
                'attribute' => 'date_create',
                'format' =>  ['date', 'yyyy.MM.dd H:m:s'],
                'options' => ['width' => '200']
            ],

            'number',

            [
                'attribute'=>'id_marka',
                'value'=>'marka.title',
                'filter' => Html::activeDropDownList($searchModel, 'id_marka',
                    ArrayHelper::map(\app\models\Marka::find()->asArray()->all(), 'id', 'title'),
                    ['class'=>'form-control', 'prompt' => '...']),
                'enableSorting' => true,
            ],
            [
                'attribute'=>'id_sklad',
                'value'=>'sklad.title',
                'filter' => Html::activeDropDownList($searchModel, 'id_sklad',
                    ArrayHelper::map(\app\models\Sklad::find()->asArray()->all(), 'id', 'title'),
                    ['class'=>'form-control', 'prompt' => '...']),
                'enableSorting' => true,
            ],
            [
                'attribute'=>'id_smena',
                'value'=>'smena.title',
                'filter' => Html::activeDropDownList($searchModel, 'id_smena',
                    ArrayHelper::map(\app\models\Smena::find()->asArray()->all(), 'id', 'title'),
                    ['class'=>'form-control', 'prompt' => '...']),
                'enableSorting' => true,
            ],

            [
                'attribute'=>'id_status',
                'value'=>'status.title',
                'filter' => Html::activeDropDownList($searchModel, 'id_status',
                    ArrayHelper::map(\app\models\Status::find()->asArray()->all(), 'id', 'title'),
                    ['class'=>'form-control', 'prompt' => '...']),
                'enableSorting' => true,
                'contentOptions' =>function ($model, $key, $index, $column) {
                    $clr = \app\models\Status::findOne($model->id_status)->color;
                    return ['style' => 'background-color:'.$clr];
                },
            ],

            'comment:ntext',

            [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],

                //'template' => '{view} {update} {delete} {all}',
                'template' => '{view} {update} {all}',

                'buttons' => [

                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => 'Подробнее',
                        ]);
                    },

                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title' => 'Редактировать',
                        ]);
                    },
/*
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['smena-delete', 'id' => $model->id], [
                                'data' => [
                                    'confirm' => 'Выдействительно хотите удалить данный продукт?',
                                    'method' => 'post',
                            ],
                        ]);
                    },
*/
                    'all' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-th-list"></span>', $url, [
                            'title' => 'История продукта',
                        ]);
                    },

                ],

                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'update') {
                        $url ='index.php?r=pack%2Fsmena-update&id='.$model->id;
                        return $url;
                    };

                    if ($action === 'view') {
                        $url ='index.php?r=pack%2Fsmena-view&id='.$model->id;
                        return $url;
                    };
/*
                    if ($action === 'delete') {
                        $url ='index.php?r=pack%2Fsmena-delete&id='.$model->id;
                        return $url;
                    };
*/
                    if ($action === 'all') {
                        $url ='index.php?r=pack%2Fall&id='.$model->id;
                        return $url;
                    };

                },

            ],
        ]
    ?>

    <?= GridView::widget([
        'dataProvider'=>$dataProvider,
        'rowOptions' => ['style' => 'background-color:white;'],
        'pjax'=>false,
        'responsive'=>true,
        'panel'=>['type'=>'primary'],

        'columns' => $dataColumns,

        'toolbar' =>  [
            '{export}',
            '{toggleData}'
        ],
        'exportConfig' => [
            GridView::EXCEL => ['label' => 'Сохранить в файл EXCEL'],
        ],
        'filterModel'=>$searchModel,
    ]); ?>

</div>
