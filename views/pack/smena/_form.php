<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\widgets\MaskedInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Pack */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pack-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date_create')->textInput(array(
        'value' => (new \DateTime('now', new \DateTimeZone('Europe/Moscow')))->format('Y-m-d H:i:s'),
        'class'=>'form-control','readonly' => true));
    ?>

    <?= $form->field($model, 'date_modify')->hiddenInput(array(
        'value' => (new \DateTime('now', new \DateTimeZone('Europe/Moscow')))->format('Y-m-d H:i:s'),
        'class'=>'form-control','readonly' => true))->label(false);
    ?>


    <?= $form->field($model, 'id_marka')
        ->dropDownList(\app\models\Marka::find()
            ->select(['title','id'])
            ->indexBy('id')
            ->column(),
            [
                'prompt' => '',
                'onchange' => '$.post(
                                      "'.Url::toRoute('pack/num-pack').'",
                                      {id : $(this).val()},
                                      function(data){
                                          $(".pack-number").val(data)
                                      }
                                )'
            ]
        )
    ?>


    <?= $form->field($model, 'number')->textInput(array(
        'maxlength' => true,
        'readonly' => true,
        'value' => 'Выберите вначале марку...',
        'class'=>'form-control pack-number'));
    ?>


    <?= $form->field($model, 'id_status')->hiddenInput(array(
        'value' => Yii::$app->params['statusDefault'],
        'class'=>'form-control','readonly' => true))->label(false);
    ?>


    <?php

    /*
        if (strpos(Yii::$app->request->queryParams['r'], 'create') == false) {
            echo $form->field($model, 'id_status')->dropDownList(\app\models\Status::find()->select(['title','id'])->indexBy('id')->column() );
        }else{
            echo $form->field($model, 'id_status')->hiddenInput(array(
                'value' => Yii::$app->params['statusDefault'],
                'class'=>'form-control','readonly' => true))->label(false);
        }
*/
    ?>



    <?= $form->field($model, 'id_sklad')->hiddenInput(['value' => 1])->label(false); ?>

    <?= $form->field($model, 'id_tu')->dropDownList(\app\models\Tu::find()->select(['title','id'])->indexBy('id')->column(),
            [
                'prompt' => ''
            ]
        ) ?>

    <?= $form->field($model, 'id_smena')->dropDownList(\app\models\Smena::find()->select(['title','id'])->indexBy('id')->column() ) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 2]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
