<?php

/* @var $this yii\web\View */


use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

$this->title = 'Учёт готовой продукции';
?>
<div class="site-index">

    <div class="body-content">
        <div class="alert alert-info">
            <h1>Готовая продукция (мешки)</h1>
        </div>
        <?  $dataColumns =
            [
                [
                    'attribute' => 'date_create',
                    'format' =>  ['date', 'yyyy.MM.dd H:m:s'],
                    'options' => ['width' => '200']
                ],

                'number',

                [
                    'attribute'=>'id_marka',
                    'value'=>'marka.title',
                    'filter' => Html::activeDropDownList($searchModel, 'id_marka',
                        ArrayHelper::map(\app\models\Marka::find()->asArray()->all(), 'id', 'title'),
                        ['class'=>'form-control', 'prompt' => '...']),
                    'enableSorting' => true,
                ],


                [
                    'attribute'=>'id_sklad',
                    'value'=>'sklad.title',
                    'filter' => Html::activeDropDownList($searchModel, 'id_sklad',
                        ArrayHelper::map(\app\models\Sklad::find()->asArray()->all(), 'id', 'title'),
                        ['class'=>'form-control', 'prompt' => '...']),
                    'enableSorting' => true,
                ],

                [
                    'attribute'=>'id_status',
                    'value'=>'status.title',
                    'filter' => Html::activeDropDownList($searchModel, 'id_status',
                        ArrayHelper::map(\app\models\Status::find()->asArray()->all(), 'id', 'title'),
                        ['class'=>'form-control', 'prompt' => '...']),
                    'enableSorting' => true,

                    'contentOptions' =>function ($model, $key, $index, $column) {
                        $clr = \app\models\Status::findOne($model->id_status)->color;
                        return ['style' => 'background-color:'.$clr];
                    },
                ],

                'comment:ntext',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],

                    'template' => '{view} {all}',

                    'buttons' => [

                        'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => 'Подробнее',
                            ]);
                        },

                        'all' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-th-list"></span>', $url, [
                                'title' => 'История продукта',
                            ]);
                        },

                    ],

                    'urlCreator' => function ($action, $model, $key, $index) {

                        if ($action === 'view') {
                            $url ='index.php?r=pack%2Fview&id='.$model->id;
                            return $url;
                        };

                        if ($action === 'all') {
                            $url ='index.php?r=pack%2Fall&id='.$model->id;
                            return $url;
                        };

                    },

                ],
            ]
        ?>

        <?= GridView::widget([
            'dataProvider'=>$dataProvider,
            'rowOptions' => ['style' => 'background-color:white;'],
            'pjax'=>false,
            'responsive'=>true,
            'panel'=>['type'=>'primary'],

            'columns' => $dataColumns,

            'toolbar' =>  [
                '{export}',
                '{toggleData}'
            ],
            'exportConfig' => [
                GridView::EXCEL => ['label' => 'Сохранить в файл EXCEL'],
            ],
            'filterModel'=>$searchModel,
        ]); ?>

    </div>


</div>
