<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Pack */
/* @var $form yii\widgets\ActiveForm */
?>
<strong class = "lab_update_status" style="background-color: <?= $model->status->color; ?>">Текущий статус: <?= $model->status->title; ?></strong><br><br>


<div class="pack-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-md-6">

            <?= $form->field($model, 'date_create')->textInput(array('class'=>'form-control',
                'readonly' => true));
            ?>

            <?= $form->field($model, 'date_modify')->textInput(array(
                'value' => (new \DateTime('now', new \DateTimeZone('Europe/Moscow')))->format('Y-m-d H:i:s'),
                'class'=>'form-control',
                'readonly' => true));
            ?>

            <?= $form->field($model, 'number')->textInput(array(
                    'readonly' => true,
                    'maxlength' => true,
                    'value' => $model->number,
                    'class'=>'form-control'));
            ?>

        </div>

        <div class="col-md-6">


            <?= $form->field($model, 'id_sklad')
                ->dropDownList(\app\models\Sklad::find()
                    ->select(['title','id'])
                    ->indexBy('id')
                    ->column(),
                    [
                        'onchange' => "
                                    $.ajax({
                                      type: 'POST',
                                      url : '".Url::toRoute('pack/change-sklad')."',
                                      cache: false,
                                      data : {value : $(this).val(), id_status : '".$model->status->id."'},
                                      success : function(data){                                                                           
                                           if (data != 0) {
                                                $('#pack-id_status').val(data);
                                                $('.lab_new_status span').html($('#pack-id_status option:selected').text());
                                           }
                                    }
                                })"
                    ]

                )
            ?>

            <div style = 'display:none' >
                <?= $form->field($model, 'id_status')->dropDownList(\app\models\Status::find()->select(['title','id'])->indexBy('id')->column()) ?>
            </div>

        </div>

    </div>

    <?= $form->field($model, 'comment')->textarea(['rows' => 2]) ?>



    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        <strong class = "lab_update_status lab_new_status">Новый статус: <span><?= $model->status->title;?></span></strong>
    </div>

    <?php ActiveForm::end(); ?>

</div>
