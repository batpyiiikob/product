<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pack */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pack-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date_create')->textInput() ?>

    <?= $form->field($model, 'date_modify')->textInput() ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_tu')->textInput() ?>

    <?= $form->field($model, 'id_marka')->textInput() ?>

    <?= $form->field($model, 'id_status')->textInput() ?>

    <?= $form->field($model, 'id_sklad')->textInput() ?>

    <?= $form->field($model, 'smena')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fhp_diam')->textInput() ?>

    <?= $form->field($model, 'fhp_massdol')->textInput() ?>

    <?= $form->field($model, 'fhp_nasyp')->textInput() ?>

    <?= $form->field($model, 'fhp_massdolprokal')->textInput() ?>

    <?= $form->field($model, 'fhp_mech')->textInput() ?>

    <?= $form->field($model, 'fhp_istir')->textInput() ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
