<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Движение продукции';
$this->params['breadcrumbs'][] = $this->title;
$idIn = $model->id; // текущий id для передачи в другую модель In - внешний для view - urlCreator
?>
<div class="pack-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="jumbotron">
        <h2>Номер мешка: <?= $model->number ?></h2>
        <h3>от <?= $model->date_create ?></h3>
        <h3>марка: <?= \app\models\Marka::findOne(['id' => $model->id_marka ])->title; ?></h3>
    </div>

    <?  $dataColumns =
        [
            'date_modify',

            [
                'attribute'=>'id_marka',
                'value'=>'marka.title',
                'enableSorting' => true,
            ],
            [
                'attribute'=>'id_sklad',
                'value'=>'sklad.title',
                'enableSorting' => true,
            ],
            [
                'attribute'=>'id_smena',
                'value'=>'smena.title',
                'enableSorting' => true,
            ],

            [
                'attribute'=>'id_status',
                'value'=>'status.title',
                'enableSorting' => true,
                'contentOptions' =>function ($model, $key, $index, $column) {
                    $clr = \app\models\Status::findOne($model->id_status)->color;
                    return ['style' => 'background-color:'.$clr];
                },
            ],

            'comment:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],

                'template' => '{view}',

                'buttons' => [

                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title' => Yii::t('app', 'view'),
                        ]);
                    },

                ],

                'urlCreator' => function ($action, $model, $key, $index) use ($idIn) {

                    if ($action === 'view') {
                        $url ='index.php?r=pack-false%2Fview&id='.$model->id.'&idIn='.$idIn;
                        return $url;
                    };

                },

            ],
        ]
    ?>

    <?= GridView::widget([
        'dataProvider'=>$dataProviderLog,
        'rowOptions' => ['style' => 'background-color:white;'],
        'pjax'=>true,
        'responsive'=>true,
        'panel'=>['type'=>'primary'],

        'columns' => $dataColumns,

        'toolbar' =>  [
            '{export}',
            '{toggleData}'
        ],
        'exportConfig' => [
            GridView::EXCEL => ['label' => 'Сохранить в файл EXCEL'],
        ],
    ]); ?>
</div>
