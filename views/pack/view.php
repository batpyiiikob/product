<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pack */

$this->title = $model->number;

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pack-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'date_create',
            'date_modify',
            'number',

            [
                'attribute' => 'marka.title',
            ],

            [
                'attribute' => 'tu.title',
            ],

            [
                'attribute' => 'smena.title',
            ],

            [
                'attribute' => 'status.title',
            ],

            [
                'attribute' => 'sklad.title',
            ],

            'fhp_diam',
            'fhp_massdol',
            'fhp_nasyp',
            'fhp_massdolprokal',
            'fhp_mech',
            'fhp_istir',
            'comment:ntext',
        ],
    ]) ?>

</div>
