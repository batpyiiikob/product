<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PackSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pack-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'date_create') ?>

    <?= $form->field($model, 'date_modify') ?>

    <?= $form->field($model, 'number') ?>

    <?= $form->field($model, 'id_tu') ?>

    <?php // echo $form->field($model, 'id_marka') ?>

    <?php // echo $form->field($model, 'id_status') ?>

    <?php // echo $form->field($model, 'id_sklad') ?>

    <?php // echo $form->field($model, 'smena') ?>

    <?php // echo $form->field($model, 'fhp_diam') ?>

    <?php // echo $form->field($model, 'fhp_massdol') ?>

    <?php // echo $form->field($model, 'fhp_nasyp') ?>

    <?php // echo $form->field($model, 'fhp_massdolprokal') ?>

    <?php // echo $form->field($model, 'fhp_udel') ?>

    <?php // echo $form->field($model, 'fhp_mech') ?>

    <?php // echo $form->field($model, 'fhp_istir') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
