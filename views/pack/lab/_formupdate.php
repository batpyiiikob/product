<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\widgets\MaskedInput;
use yii\helpers\Url;
use app\models\FhpLim;

/* @var $this yii\web\View */
/* @var $model app\models\Pack */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pack-form">


    <strong class = "lab_update_status" style="background-color: <?= $model->status->color; ?>">Текущий статус: <?= $model->status->title; ?></strong><br><br>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-md-6">

            <?= $form->field($model, 'date_create')->textInput(array('class'=>'form-control',
                                     'readonly' => true));
            ?>

            <?= $form->field($model, 'date_modify')->textInput(array(
                'value' => (new \DateTime('now', new \DateTimeZone('Europe/Moscow')))->format('Y-m-d H:i:s'),
                'class'=>'form-control',
                'readonly' => true));
            ?>

            <?= $form->field($model, 'number')->textInput(array(
                     'readonly' => true,
                     'maxlength' => true,
                     'value' => $model->number,
                     'class'=>'form-control pack-id_status'));
            ?>

            <div style = 'display:none' >
                <?= $form->field($model, 'id_status')->dropDownList(\app\models\Status::find()->select(['title','id'])->indexBy('id')->column()); ?>
            </div>


            <?php //echo $form->field($model, 'id_status')->dropDownList(\app\models\Status::find()->select(['title','id'])->where(['id' => [2,3]])->indexBy('id')->column())->label('Новый статус');
            ?>

            <?= $form->field($model, 'comment')->textarea(['rows' => 2]) ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                <strong class = "lab_update_status lab_new_status">Новый статус: <span><?= $model->status->title;?></span></strong>
            </div>


        </div>








        <div class="col-md-6">

            <?= $form->field($model, 'id_tu')->dropDownList(\app\models\Tu::find()->select(['title','id'])->indexBy('id')->column(),
                [
                    'disabled' => true,
                ] )
            ?>


            <?=$form->field($model, 'fhp_diam',[
                'template' => "{label}".$labels['diam']."\n{input}\n{hint}\n{error}"
            ])->textInput(
                [
                    'onkeyup' => "$.ajax({
                                      type: 'POST',
                                      url : '".Url::toRoute('tu/field-fhp-status')."',
                                      cache: false,
                                      data : {value : $(this).val(), field : 'diam', id : '".$model->id."', tu : '".$model->tu->id."', marka : '".$model->marka->id."'},
                                      success : function(data){ onKeyFHP(data, $('.field-pack-fhp_diam'), '".Yii::$app->params['paramTuFalse']."'); }
                                })"
                ]
            )
            ?>


            <?= $form->field($model, 'fhp_massdol',[
                    'template' => "{label}".$labels['massdol']."\n{input}\n{hint}\n{error}"
            ])->textInput(
                [
                    'onkeyup' => "$.ajax({
                                      type: 'POST',
                                      url : '".Url::toRoute('tu/field-fhp-status')."',
                                      cache: false,
                                      data : {value : $(this).val(), field : 'massdol', id : '".$model->id."', tu : '".$model->tu->id."', marka : '".$model->marka->id."'},
                                      success : function(data){ onKeyFHP(data, $('.field-pack-fhp_massdol'), '".Yii::$app->params['paramTuFalse']."'); }
                                 })"
                ]
            )
            ?>

            <?= $form->field($model, 'fhp_nasyp',[
                'template' => "{label}".$labels['nasyp']."\n{input}\n{hint}\n{error}"
            ])->textInput(
                [
                    'onkeyup' => "$.ajax({
                                      type: 'POST',
                                      url : '".Url::toRoute('tu/field-fhp-status')."',
                                      cache: false,
                                      data : {value : $(this).val(), field : 'nasyp', id : '".$model->id."', tu : '".$model->tu->id."', marka : '".$model->marka->id."'},
                                      success : function(data){ onKeyFHP(data, $('.field-pack-fhp_nasyp'), '".Yii::$app->params['paramTuFalse']."'); }
                                })"
                ]
            )
            ?>

            <?= $form->field($model, 'fhp_massdolprokal',[
                'template' => "{label}".$labels['massdolprokal']."\n{input}\n{hint}\n{error}"
            ])->textInput(
                [
                    'onkeyup' => "$.ajax({
                                      type: 'POST',
                                      url : '".Url::toRoute('tu/field-fhp-status')."',
                                      cache: false,
                                      data : {value : $(this).val(), field : 'massdolprokal', id : '".$model->id."', tu : '".$model->tu->id."', marka : '".$model->marka->id."'},
                                      success : function(data){ onKeyFHP(data, $('.field-pack-fhp_massdolprokal'), '".Yii::$app->params['paramTuFalse']."'); }
                                })"
                ]
            )
            ?>


<!--
**************************************************
Удельная поверхность (begin)
**************************************************
-->
            <?= $form->field($model, 'fhp_udel_azot',[
                'template' => "{label}".$labels['udel_azot']."\n{input}\n{hint}\n{error}"
            ])->textInput(
                [
                    'onkeyup' => "$.ajax({
                                      type: 'POST',
                                      url : '".Url::toRoute('tu/field-fhp-udel-status')."',
                                      cache: false,
                                      data : {
                                                ar : $('input#pack-fhp_udel_argon').val(),
                                                az : $(this).val(),
                                                tu : '".$model->tu->id."',
                                                marka : '".$model->marka->id."'
                                             },  
                                      success : function(data){                                                
                                                onKeyFHPUdel(jQuery.parseJSON(data));
                                             }
                                })"
                ]
            )
            ?>

            <?= $form->field($model, 'fhp_udel_argon',[
                'template' => "{label}".$labels['udel_argon']."\n{input}\n{hint}\n{error}"
            ])->textInput(
                [
                    'onkeyup' => "$.ajax({
                                      type: 'POST',
                                      url : '".Url::toRoute('tu/field-fhp-udel-status')."',
                                      cache: false,
                                      data : {
                                                ar : $(this).val(), 
                                                az : $('input#pack-fhp_udel_azot').val(), 
                                                tu : '".$model->tu->id."', 
                                                marka : '".$model->marka->id."'
                                              },
                                      success : function(data){         
                                                onKeyFHPUdel(jQuery.parseJSON(data)); 
                                              }                                 
                                })"
                ]
            )
            ?>
<!--
**************************************************
Удельная поверхность (end)
**************************************************
-->

            <?= $form->field($model, 'fhp_mech',[
                'template' => "{label}".$labels['mech']."\n{input}\n{hint}\n{error}"
            ])->textInput(
                [
                    'onkeyup' => "$.ajax({
                                      type: 'POST',
                                      url : '".Url::toRoute('tu/field-fhp-status')."',
                                      cache: false,
                                      data : {value : $(this).val(), field : 'mech', id : '".$model->id."', tu : '".$model->tu->id."', marka : '".$model->marka->id."'},
                                      success : function(data){ onKeyFHP(data, $('.field-pack-fhp_mech'), '".Yii::$app->params['paramTuFalse']."'); }  
                                })"
                ]
            )
            ?>

            <?= $form->field($model, 'fhp_istir',[
                'template' => "{label}".$labels['istir']."\n{input}\n{hint}\n{error}"
            ])->textInput(
                [
                    'onkeyup' => "$.ajax({
                                      type: 'POST',
                                      url : '".Url::toRoute('tu/field-fhp-status')."',
                                      cache: false,
                                      data : {value : $(this).val(), field : 'istir', id : '".$model->id."', tu : '".$model->tu->id."', marka : '".$model->marka->id."'},
                                      success : function(data){ onKeyFHP(data, $('.field-pack-fhp_istir'), '".Yii::$app->params['paramTuFalse']."'); }
                                })"
                ]
            )
            ?>

            <?= $form->field($model, 'fhp_comob',[
                'template' => "{label}".$labels['comob']."\n{input}\n{hint}\n{error}"
            ])->textInput(
                [
                    'onkeyup' => "$.ajax({
                                      type: 'POST',
                                      url : '".Url::toRoute('tu/field-fhp-status')."',
                                      cache: false,
                                      data : {value : $(this).val(), field : 'comob', id : '".$model->id."', tu : '".$model->tu->id."', marka : '".$model->marka->id."'},
                                      success : function(data){ onKeyFHP(data, $('.field-pack-fhp_comob'), '".Yii::$app->params['paramTuFalse']."'); }                 
                                })"
                ]
            )
            ?>

        </div>

    </div>


    <?php ActiveForm::end(); ?>

</div>




