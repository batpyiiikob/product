<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pack */

$this->title = 'Редактировать продукт: '.$model->number.' - ('.$model->sklad->title.')';
$this->params['breadcrumbs'][] = ['label' => 'Мешки', 'url' => ['lab']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pack-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formupdate',  [
        'model' => $model,
        'labels' => $labels,
    ]) ?>

</div>
