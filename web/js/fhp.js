/**
 * Created by BATPYIIIKOB on 01.08.2018.
 */

//$(document).ready(function () {
//});


// Срабатывает когда загружен элемент
//$('#pack-fhp_massdol').ready(function (){
//});


/*
*
* data - массив соответствие ФХП (ar, az)
* field - изменяемое поле
*
* */
function onKeyFHPUdel(obj){

    var paramTuFalse  = '<span class="error-fhp">Параметр не соответствует ТУ</span>';
    var paramTuFalseUdel  = '<span class="error-fhp-udel">Параметр не соответствует ТУ</span>';

    lblArgon = $('.field-pack-fhp_udel_argon');   //подпись аргона
    lblAzot  = $('.field-pack-fhp_udel_azot');    //подпись азота

    lblArgon.find('span').remove();
    lblAzot.find('span').remove();

    var arr = jQuery.makeArray(obj);

    // 0 - false, 1 - true, 2 - null

    if (arr["0"].ar == 2){
        if (arr["0"].az == 1){

            lblArgon.find('.control-label').append(paramTuFalseUdel);

        } else if (arr["0"].az== 0){

            lblArgon.find('.control-label').append(paramTuFalseUdel);
            lblAzot.find('.control-label').append(paramTuFalse);

        } else{

            lblArgon.find('.control-label').append(paramTuFalse);
            lblAzot.find('.control-label').append(paramTuFalseUdel);

        }
    }


    if (arr["0"].ar == 1){
        if (arr["0"].az == 2){

            lblAzot.find('.control-label').append(paramTuFalseUdel);

        } else if (arr["0"].az == 0){

            lblAzot.find('.control-label').append(paramTuFalseUdel);

        }
    }


    if (arr["0"].ar == 0){
        if (arr["0"].az == 1){

            lblArgon.find('.control-label').append(paramTuFalse);

        } else
        {
            lblArgon.find('.control-label').append(paramTuFalse);
            lblAzot.find('.control-label').append(paramTuFalseUdel);

        }
    }

    changeStatusPack();
}





/**
 * Функция при отработке succes Ajax при проверке на ошибки ФХП в редактирование для лаборатории
 *
 * @param array data;
 * @param object fieldPackFHP;
 * @param string message;
 *
 */
function onKeyFHP(data, fieldPackFHP, message){
    changeErrorLabelElement(data, fieldPackFHP, message);
    changeStatusPack();
}


/**
 * Процедура изменения подписи при проверке на ошибки ФХП в редактирование для лаборатории
 *
 * @param array data;
 * @param object fieldPackFHP;
 * @param string message;
 *
 */
function changeErrorLabelElement(data, fieldPackFHP, message) {
    if (data == true){
        fieldPackFHP.find('span').remove();
    }else{
        if (!fieldPackFHP.find('span').length)
        {
            fieldPackFHP.find('.control-label').append(message);
        }
    }
}


/**
 * Процедура изменения статуса ФХП в редактирование для лаборатории
 */
function changeStatusPack(){
    if ($('span').is('.error-fhp')){
        $('#pack-id_status').val(3);
        $('.lab_new_status span').html($('#pack-id_status option:selected').text());
    }else{
        $('#pack-id_status').val(2);
        $('.lab_new_status span').html($('#pack-id_status option:selected').text());
    }
}
